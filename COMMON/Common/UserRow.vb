﻿Public Class UserRow
    ''' <summary>
    ''' ID。
    ''' </summary>
    Public ReadOnly Property Id As String

    ''' <summary>
    ''' 漢字名称。
    ''' </summary>
    Public ReadOnly Property Kanji As String

    ''' <summary>
    ''' カナ名称。
    ''' </summary>
    Public ReadOnly Property Kana As String

    ''' <summary>
    ''' 所属課コード。
    ''' </summary>
    Public ReadOnly Property SectionCode As String

    ''' <summary>
    ''' 所属課名前。
    ''' </summary>
    Public ReadOnly Property SectionName As String

    ''' <summary>
    ''' アクセルロール。
    ''' </summary>
    Public ReadOnly Property Accesslv As String
    ''' <summary>
    ''' ロール番号。
    ''' </summary>
    Public ReadOnly Property RoleId As String
    ''' <summary>
    ''' 課長ユーザーコード。
    ''' </summary>
    Public ReadOnly Property SectionUserCode As String

    ''' <summary>
    ''' Initializes a new instance of the <see cref="UserRow" /> class.
    ''' </summary>
    ''' <param name="id">ユーザーID。</param>
    ''' <param name="kanji">漢字名称。</param>
    ''' <param name="kana">カナ名称。</param>
    ''' <param name="SectionCode">所属課コード。</param>
    ''' <param name="SectionName">所属課名称。</param>
    ''' <param name="Accesslv">アクセルロール。</param>
    ''' <param name="RoleId">ロール番号。</param>
    ''' <param name="SectionUserCode">課長ユーザーコード。</param>
    Public Sub New(ByVal id As String, ByVal kanji As String, ByVal kana As String, ByVal SectionCode As String, ByVal SectionName As String, ByVal Accesslv As String, ByVal RoleId As String, ByVal SectionUserCode As String)
        Me.Id = id
        Me.Kanji = kanji
        Me.Kana = kana
        Me.SectionCode = SectionCode
        Me.SectionName = SectionName
        Me.Accesslv = Accesslv
        Me.RoleId = RoleId
        Me.SectionUserCode = SectionUserCode
    End Sub

End Class
