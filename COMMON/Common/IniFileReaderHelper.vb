﻿Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text.RegularExpressions

Public Class IniFileReaderHelper

#Region "メンバ"
    Private Property INI_FILE_PATH As String
#End Region
#Region "指定のINIファイルから文字列を取得する"
    <DllImport("KERNEL32.DLL", CharSet:=CharSet.Auto)>
    Public Shared Function GetPrivateProfileString(
    ByVal lpAppName As String,
    ByVal lpKeyName As String, ByVal lpDefault As String,
    ByVal lpReturnedString As System.Text.StringBuilder, ByVal nSize As Integer,
    ByVal lpFileName As String) As Integer
    End Function
#End Region
#Region "指定のINIファイルの指定のキーの文字列を変更する"
    <DllImport("KERNEL32.DLL", CharSet:=CharSet.Auto)>
    Public Shared Function WritePrivateProfileString(
        ByVal lpApplicationName As String,
        ByVal lpKeyName As String,
        ByVal lpString As String,
        ByVal lpFileName As String) As Long
    End Function
#End Region
#Region "コンストラクタ"
    ''' <summary>
    ''' コンストラクタ(初期処理)
    ''' </summary>
    Sub New()
        Me.INI_FILE_PATH = My.Settings.exePath + My.Settings.iniFileName
    End Sub
#End Region
#Region "INIファイル情報取得"
    ''' <summary>
    ''' 構成ファイルの値取得
    ''' </summary>
    ''' <param name="Section">セクション</param>
    ''' <param name="Key">キー</param>
    ''' <param name="DefaultValue">データがない場合、ディフォルト値</param>
    ''' <returns>iniファイル値</returns>
    Public Function GetIniValue(Section As String, Key As String, Optional DefaultValue As String = "") As String
        ' 戻り値変数
        Dim rtnValue As System.Text.StringBuilder = New System.Text.StringBuilder(1024)
        Dim sLen = GetPrivateProfileString(Section, Key, DefaultValue, rtnValue, 1024, Me.INI_FILE_PATH)
        Dim str As String = rtnValue.ToString()
        Return str
    End Function
#End Region
#Region "指定のINIファイルの指定のキーの文字列を変更する"
    ''' <summary>
    ''' 構成ファイルの値変更
    ''' </summary>
    ''' <param name="Section">セクション</param>
    ''' <param name="Key">キー</param>
    ''' <param name="Value">変更値</param>
    ''' <returns>変更成功：True 変更失敗:False</returns>
    Public Function PutIniValue(ByVal Section As String, Key As String, ByVal Value As String) As Boolean
        Dim result As Long = WritePrivateProfileString(Section, Key, Value, INI_FILE_PATH)
        Return result <> 0
    End Function
#End Region

End Class
