﻿Imports System.IO
Imports System.Windows.Forms

Public Class Common

    ''' <summary>
    ''' ２重起動防止Application.ProductName
    ''' </summary>
    Public Function Init() As Boolean
        ' ログ
        LogHelper.writeLog(Application.ProductName + " 開始 " + System.Reflection.MethodBase.GetCurrentMethod().Name)

        Static mutex As New System.Threading.Mutex(True, Application.ProductName)
        Dim hasHandle As Boolean = False
        Try
            Try
                ' ミューテックスの所有権を要求する
                hasHandle = mutex.WaitOne(0, False)
            Catch ex As Exception
                '別のアプリケーションがミューテックスを解放しないで終了した時
                hasHandle = True
            End Try

            'ミューテックスを得られたか調べる
            If hasHandle = False Then
                LogHelper.WriteLogAndShowMsg(New Exception("既に起動しています。"), "既に起動しています。", Application.ProductName)
                Return False
            End If

            Return True
        Catch ex As Exception
            Throw ex
            Return False
        Finally
            If hasHandle = True Then
                ' ミューテックスを解放する
                mutex.ReleaseMutex()
            End If
        End Try
    End Function

    ''' <summary>
    ''' フォルダ選択ダイアログ
    ''' </summary>
    Public Function browseFolder() As String
        Dim path As String = ""
        Try

            ' FolderBrowserDialogクラスのインスタンスを作成
            Dim fbd As New FolderBrowserDialog

            ' 上部に表示する説明テキストを指定する
            fbd.Description = "フォルダを指定してください。"

            ' ルートフォルダを指定する
            ' デフォルトでDesktop
            fbd.RootFolder = Environment.SpecialFolder.Desktop

            ' 最初に選択するフォルダを指定する
            ' RootFolder以下にあるフォルダである必要がある
            'fbd.SelectedPath = @"C:\Windows";

            ' ユーザーが新しいフォルダを作成できるようにする
            ' デフォルトでTrue
            fbd.ShowNewFolderButton = True

            ' ダイアログを表示する
            If fbd.ShowDialog() = DialogResult.OK Then
                ' 選択されたフォルダを表示する
                ' Console.WriteLine(fbd.SelectedPath)
                path = fbd.SelectedPath
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return path
    End Function
    ''' <summary>
    ''' exeファイルパス取得
    ''' </summary>
    ''' <returns></returns>
    Public Function GetexePath() As String
        Return My.Settings.exePath
        ' Return Directory.GetCurrentDirectory()
    End Function
    ''' <summary>
    ''' メッセージ ボックスのタイトル バーに表示するテキスト取得
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GetCaption() As String
        Dim init = New IniFileReaderHelper
        Return init.GetIniValue("COMMON", "Caption", "")
    End Function

    Public Shared Function GetAccessLv() As String
        Dim init = New IniFileReaderHelper
        Return init.GetIniValue("USERINFO", "login_access_lv", "")
    End Function
End Class
