﻿Public MustInherit Class UserHelperBase
    Private USERINFO As UserInfo


    ''' <summary>
    '''コンストラクタ
    ''' </summary>
    Sub New()
        Dim userId = GetUserId(Environment.UserName.ToString)
        Dim userRow = GetUserRow(userId)
        Me.USERINFO = New UserInfo(userRow, userRow)
    End Sub

    ''' <summary>
    '''ユーザー情報取得
    ''' </summary>
    ''' <returns>ユーザー情報構造体</returns>
    Public Function GetuserInfo() As UserInfo
        Return USERINFO
    End Function

    ''' <summary>
    ''' ログイン処理を実施する。
    ''' </summary>
    ''' <param name="userId">ログインユーザーID。</param>
    ''' <param name="password">パスワード。</param>
    ''' <returns>ログインした場合True。</returns>
    Public Function Login(userId As String, password As String) As Boolean
        Dim ad = New ActiveDirectoryConnect()
        If ad.ExistsUser(userId, password) Then
            Dim baseUserId = GetUserId(Environment.UserName.ToString)
            Dim basicUserRow = GetUserRow(baseUserId)
            Dim loginUser = GetUserRow(userId)
            If loginUser.Id.Length > 0 Then
                Me.USERINFO = New UserInfo(basicUserRow, loginUser)
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    ''' <summary>
    ''' ユーザーIDからユーザー情報をを取得する。
    ''' </summary>
    ''' <param name="userId">ユーザーID。</param>
    ''' <returns>ユーザー情報。</returns>
    Private Function GetUserRow(ByVal userId As String) As UserRow
        Dim windowSize As New IniFileReaderHelper
        Dim isDebug As Integer = windowSize.GetIniValue("DEBUG", "isDebug", "")
        'iniファイルの設定はDEBUGモードの場合
        'If isDebug = 1 Then
        '    '開発環境
        '    Dim user_id As String = windowSize.GetIniValue("DEBUG", "user_id", "")
        '    Dim user_name As String = windowSize.GetIniValue("DEBUG", "user_name", "")
        '    Dim user_name_kana As String = windowSize.GetIniValue("DEBUG", "user_name_kana", "")
        '    Dim section_code As String = windowSize.GetIniValue("DEBUG", "section_code", "")
        '    Dim section_name As String = windowSize.GetIniValue("DEBUG", "section_name", "")
        '    Dim access_lv As String = windowSize.GetIniValue("DEBUG", "access_lv", "")
        '    Dim role_id As String = windowSize.GetIniValue("DEBUG", "role_id", "")
        '    Return New UserRow(user_id, user_name, user_name_kana, section_code, section_name, access_lv, role_id)
        '    'Return New UserRow(String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
        'Else
        ' 本番環境
        Dim userData = GetUserData(userId)
        Dim access_lv As String
        If (userData.Rows.Count > 0) Then
            access_lv = IIf(userData.Rows(0)("user_id").ToString().Equals(userData.Rows(0)("section_user_id").ToString()), "2", "1")
            Return New UserRow(userData.Rows(0)("user_id").ToString(), userData.Rows(0)("user_name").ToString(), userData.Rows(0)("user_name_kana").ToString(), userData.Rows(0)("section_code").ToString(), userData.Rows(0)("section_name").ToString(), access_lv, userData.Rows(0)("ROLE_ID").ToString(), userData.Rows(0)("section_user_id").ToString())
        Else
            Return New UserRow(String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty)
        End If
        'End If
    End Function

    ''' <summary>
    ''' ADのIdentity.NameからUserID部分を取得する。
    ''' </summary>
    ''' <param name="adUserName">Identity.Name。</param>
    ''' <returns>ユーザーID。</returns>
    Private Function GetUserId(ByVal adUserName As String) As String
        If adUserName.Split("\").Count > 1 Then
            ' ADログイン時（YMS21\account）
            Return adUserName.Split("\")(1)
        Else
            'Workgroupログイン時：開発環境（account）
            Return adUserName.Split("\")(0)
        End If
    End Function

    ''' <summary>
    ''' ユーザー情報を取得する。
    ''' </summary>
    ''' <param name="userId">ユーザーID。</param>
    ''' <returns>ユーザー情報。</returns>
    Protected MustOverride Function GetUserData(userId As String) As DataTable

    ''' <summary>
    ''' ADユーザーID取得
    ''' </summary>
    ''' <returns></returns>
    Public Function GetUserId() As String
        Try
            Dim init As IniFileReaderHelper = New IniFileReaderHelper
            Dim userId As String = init.GetIniValue("USERINFO", "login_user_id")
            If Len(userId) > 0 Then
                Return userId
            End If
            ' ユーザー情報取得
            Dim userHelper = New UserHelper()
            Return userHelper.GetuserInfo().LoginUser.Id
        Catch ex As Exception
            ' ログ出力
            LogHelper.writeLog(System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message, "ERROR")
            Return String.Empty
        End Try
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="userInfor"></param>
    ''' <returns></returns>
    Public Function SetUserInfo(userInfor As UserInfo) As Boolean
        Try
            Dim init As IniFileReaderHelper = New IniFileReaderHelper
            ' ADユーザー情報設定
            init.PutIniValue("USERINFO", "ad_user_id", userInfor.BasicUser.Id) ' ユーザーID
            init.PutIniValue("USERINFO", "ad_user_name", userInfor.BasicUser.Kanji) ' ユーザー名（漢字）
            init.PutIniValue("USERINFO", "ad_user_name_kana", userInfor.BasicUser.Kana) ' ユーザー名（かな）
            init.PutIniValue("USERINFO", "ad_section_code", userInfor.BasicUser.SectionCode) ' 課コード
            init.PutIniValue("USERINFO", "ad_section_name", userInfor.BasicUser.SectionName) ' 課名
            init.PutIniValue("USERINFO", "ad_access_lv", userInfor.BasicUser.Accesslv) ' アクセスレベル
            init.PutIniValue("USERINFO", "ad_role_id", userInfor.BasicUser.RoleId) ' ロール
            init.PutIniValue("USERINFO", "ad_section_user_id", userInfor.BasicUser.SectionUserCode) ' 課長ID

            ' Loginユーザー情報設定
            init.PutIniValue("USERINFO", "login_user_id", userInfor.LoginUser.Id) ' ユーザーID
            init.PutIniValue("USERINFO", "login_user_name", userInfor.LoginUser.Kanji) ' ユーザー名（漢字）
            init.PutIniValue("USERINFO", "login_user_name_kana", userInfor.LoginUser.Kana) ' ユーザー名（かな）
            init.PutIniValue("USERINFO", "login_section_code", userInfor.LoginUser.SectionCode) ' 課コード
            init.PutIniValue("USERINFO", "login_section_name", userInfor.LoginUser.SectionName) ' 課名
            init.PutIniValue("USERINFO", "login_access_lv", userInfor.LoginUser.Accesslv) ' アクセスレベル
            init.PutIniValue("USERINFO", "login_role_id", userInfor.LoginUser.RoleId) ' ロール
            init.PutIniValue("USERINFO", "login_section_user_id", userInfor.LoginUser.SectionUserCode) ' 課長ID

            Return True
        Catch ex As Exception
            ' ログ出力
            LogHelper.writeLog(System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message, "ERROR")
            Return False
        End Try
    End Function
End Class
