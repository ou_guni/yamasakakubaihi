﻿Imports System.DirectoryServices

Public Class ActiveDirectoryConnect
    ''' <summary>
    ''' LDAPパスのConfig名。
    ''' </summary>
    Private LDAP_PATH As String

    Sub New()
        Dim ldap As New Global.Common.IniFileReaderHelper
        Me.LDAP_PATH = ldap.GetIniValue("COMMON", "LDAPPath", "LDAP://yms21.yamasa.com/DC=yms21,DC=yamasa,DC=com")
    End Sub

    ''' <summary>
    ''' ADユーザーの存在を確認する。
    ''' </summary>
    ''' <param name="userId">ユーザーID。</param>
    ''' <param name="password">パスワード。</param>
    ''' <returns>存在する場合True。</returns>
    Public Function ExistsUser(userId As String, password As String) As Boolean

        If LDAP_PATH Is Nothing Then
            Throw New Exception("LDAPパスが設定されていません")
        End If

        Dim ds = New DirectoryEntry(LDAP_PATH, userId, password)

        Try
            Dim obj = ds.NativeObject
            Return True
        Catch
            Return False
        End Try
    End Function
End Class
