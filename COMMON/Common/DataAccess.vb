﻿Imports System.Data.SqlClient
Public Class DataAccess
    Implements IDisposable

#Region "メンバ"

    Private DISPOSED_VALUE As Boolean
    Private DISPOSED As Boolean
    Private CONNECTION As SqlConnection
    Private TRANSACTION As SqlTransaction
    Private CONNECTIONSTRING As String
#End Region

#Region "コンストラクタ"
    Public Sub New()
        Dim iniFilereader As IniFileReaderHelper = New IniFileReaderHelper()
        Me.DISPOSED_VALUE = False
        If iniFilereader.GetIniValue("DEBUG", "isDebug") = 1 Then
            Me.CONNECTIONSTRING = My.Settings.DbString.Replace("{IP}", iniFilereader.GetIniValue("DEBUG", "DBServer_dev"))
            Me.CONNECTION = New SqlConnection()
        Else
            Me.CONNECTIONSTRING = My.Settings.DbString.Replace("{IP}", iniFilereader.GetIniValue("COMMON", "DBServer"))
            Me.CONNECTION = New SqlConnection()
        End If
    End Sub

#End Region
    ''' <summary>
    ''' コネクションをOpenする。
    ''' </summary>
    Public Sub Open()
        Me.CONNECTION.ConnectionString = Me.CONNECTIONSTRING
        Me.CONNECTION.Open()
    End Sub

    ''' <summary>
    ''' コネクションをCloseする。
    ''' </summary>
    Public Sub Close()
        If Me.CONNECTION IsNot Nothing And Me.CONNECTION.State = ConnectionState.Open Then
            Me.CONNECTION.Close()
        End If
    End Sub

    ''' <summary>
    ''' DataSetを取得する。
    ''' </summary>
    ''' <param name="command">SQLCommand。</param>
    ''' <returns>DataSet。</returns>
    Public Function GetDataSet(command As SqlCommand) As DataSet
        Dim ds = New DataSet()
        command.Connection = Me.CONNECTION
        If Me.TRANSACTION IsNot Nothing Then
            command.Transaction = Me.TRANSACTION
        End If

        Using da = New SqlDataAdapter(command)
            da.Fill(ds, "LIST")
        End Using

        Return ds
    End Function

    ''' <summary>
    ''' DataTableを取得する。
    ''' </summary>
    ''' <param name="command">SQLCommand。</param>
    ''' <returns>DataTable。</returns>
    Public Function GetDataTable(command As SqlCommand) As DataTable
        Dim dt = New DataTable()
        command.Connection = Me.CONNECTION
        If Me.TRANSACTION IsNot Nothing Then
            command.Transaction = Me.TRANSACTION
        End If

        Using da = New SqlDataAdapter(command)
            da.Fill(dt)
        End Using

        Return dt
    End Function

    ''' <summary>
    ''' SqlDataReaderを取得する。
    ''' </summary>
    ''' <param name="command">SQLCommand。</param>
    ''' <returns>SqlDataReader。</returns>
    Public Function ExecuteReader(command As SqlCommand) As SqlDataReader
        command.Connection = Me.CONNECTION
        If Me.TRANSACTION IsNot Nothing Then
            command.Transaction = Me.TRANSACTION
        End If

        Return command.ExecuteReader()

    End Function

    ''' <summary>
    ''' SQLを実行する。
    ''' </summary>
    ''' <param name="command">SQLCommand。</param>
    ''' <returns>影響を受ける行数。</returns>
    Public Function ExecuteNonQuery(command As SqlCommand) As Integer
        command.Connection = Me.CONNECTION
        If Me.TRANSACTION IsNot Nothing Then
            command.Transaction = Me.TRANSACTION
        End If

        Return command.ExecuteNonQuery()

    End Function

    ''' <summary>
    ''' SQLを実行する。
    ''' </summary>
    ''' <param name="command">SQLCommand。</param>
    ''' <returns>結果セットの最初の行。</returns>
    Public Function ExecuteScalar(command As SqlCommand) As Object
        command.Connection = Me.CONNECTION
        If Me.TRANSACTION IsNot Nothing Then
            command.Transaction = Me.TRANSACTION
        End If

        Return command.ExecuteScalar()

    End Function

    ''' <summary>
    ''' バルクコピーを実行する。
    ''' </summary>
    ''' <param name="data">コピーデータ。</param>
    ''' <param name="targetTableName">対象テーブル名。</param>
    ''' <param name="options">コピーオプション。</param>
    Public Sub BulkCopyDataTable(data As DataTable, targetTableName As String, options As SqlBulkCopyOptions)
        Using bulkCopy = New SqlBulkCopy(Me.CONNECTION, options, Me.TRANSACTION)

            bulkCopy.DestinationTableName = targetTableName
            bulkCopy.WriteToServer(data)
        End Using

    End Sub

    ''' <summary>
    ''' トランザクションを開始する。
    ''' </summary>
    ''' <returns>トランザクションオブジェクト。</returns>
    Public Function BeginTransaction() As SqlTransaction
        Me.TRANSACTION = CONNECTION.BeginTransaction()
        Return Me.TRANSACTION
    End Function

    ''' <summary>
    ''' Commitを実行する。
    ''' </summary>
    Public Sub Commit()
        If Me.TRANSACTION IsNot Nothing Then
            Me.TRANSACTION.Commit()
        End If
    End Sub

    ''' <summary>
    ''' Rollbackを実行する。
    ''' </summary>
    Public Sub Rollback()
        If Me.TRANSACTION IsNot Nothing Then
            Me.TRANSACTION.Rollback()
        End If
    End Sub

#Region "IDisposable Support"
    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not DISPOSED_VALUE Then
            If disposing Then
                If (Me.TRANSACTION IsNot Nothing) Then
                    Me.TRANSACTION.Dispose()
                    Me.TRANSACTION = Nothing
                End If

                If (Me.CONNECTION IsNot Nothing) Then
                    Me.Close()
                    Me.CONNECTION.Dispose()
                    Me.CONNECTION = Nothing
                End If
            End If
        End If

        DISPOSED_VALUE = True
    End Sub

    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
