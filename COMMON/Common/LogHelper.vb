﻿Imports System.Windows.Forms
Imports log4net

Public Class LogHelper

    Private LOGER As ILog

    ''' <summary>
    ''' コンストラクタ(初期処理)
    ''' </summary>
    Sub New()
        Try
            'log4net.GlobalContext.Properties.Item("LogName") = "C:\develop\yamasakakubaihi\" & UserID
            'log4net.GlobalContext.Properties.Item("LogName") = "C:\develop\yamasakakubaihi\LOG\" & UserID
            ' ユーザー情報取得
            Dim userHelper = New UserHelper()
            log4net.GlobalContext.Properties.Item("LogName") = userHelper.GetUserId
            Me.LOGER = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().Name)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' ログ出力
    ''' </summary>
    ''' <param name="LogMessage">ログメッセージ</param>
    ''' <param name="Level">ログレベル（DEBUG、INFO、WARN、ERROR、FATAL）</param>
    Public Shared Sub writeLog(LogMessage As String, Optional ByVal Level As String = "")
        Try
            Dim log As LogHelper = New LogHelper()
            Select Case Level
                Case "DEBUG"
                    log.LOGER.Debug(LogMessage)
                Case "INFO"
                    log.LOGER.Info(LogMessage)
                Case "WARN"
                    log.LOGER.Warn(LogMessage)
                Case "ERROR"
                    log.LOGER.Error(LogMessage)
                Case "FATAL"
                    log.LOGER.Fatal(LogMessage)
                Case Else
                    log.LOGER.Info(LogMessage)
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' ログ出力＆Messagebox
    ''' </summary>
    ''' <param name="ex">エクセプション</param>
    ''' <param name="showMessage">メッセージ ボックスに表示するテキスト。 </param>
    ''' <param name="caption">メッセージ ボックスのタイトル バーに表示するテキスト。</param>
    Public Shared Sub WriteLogAndShowMsg(ex As Exception, Optional ByVal showMessage As String = "", Optional ByVal caption As String = "", Optional ByVal boxIcon As MessageBoxIcon = MessageBoxIcon.Hand)
        Try
            ' メッセージ ボックスに表示するテキストはブランクの場合、エクセプション内容設定
            Dim init = New IniFileReaderHelper
            showMessage = IIf(String.Empty.Equals(showMessage), ex.Message, showMessage)
            caption = IIf(String.Empty.Equals(caption), init.GetIniValue("COMMON", "Caption", ""), caption)
            ' エラーログ出力
            writeLog(ex.StackTrace + vbNewLine + "   " + ex.Message, "ERROR")
            'メッセージ表示
            MessageBox.Show(showMessage, caption, MessageBoxButtons.OK, boxIcon)
        Catch exception As Exception
            Throw exception
        End Try
    End Sub

End Class
