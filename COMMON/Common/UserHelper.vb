﻿Public Class UserHelper
    Inherits UserHelperBase
    ''' <summary>
    ''' ユーザー情報を取得する。
    ''' </summary>
    ''' <param name="userId">ユーザーID。</param>
    ''' <returns>ユーザー情報。</returns>
    Protected Overrides Function GetUserData(userId As String) As DataTable
        Using da = New DataAccess()
            da.Open()

            'ユーザー情報取得用SQL
            Dim sql As String = ""
            sql = ""
            sql += " SELECT A.SECTION_CODE,"
            sql += "       C.SECTION_SHORT_NAME SECTION_NAME,"
            sql += "       A.USER_ID     ,"
            sql += "       A.USER_NAME   ,"
            sql += "      '' USER_NAME_KANA   ,"
            sql += "       isnull(B.ROLE_ID,'Z') ROLE_ID, "
            sql += "       A.section_user_id"
            sql += " FROM BRIDGE.DBO.M_USER    A "
            sql += " LEFT JOIN TM_ROLE_OF_SECTION   B "
            sql += " ON A.SECTION_CODE=b.SECTION_CODE"
            sql += " AND B.COMPANY_CODE = '01' "
            sql += " LEFT JOIN BRIDGE.DBO.M_SECTION C"
            sql += " ON A.SECTION_CODE=C.SECTION_CODE"
            sql += " WHERE A.USER_ID = @USER_ID"

            Using command = New SqlCommandManager(sql)
                command.AddParameter("@USER_ID", SqlDbType.VarChar, userId)
                Return da.GetDataTable(command.GetCommand())
            End Using
        End Using
    End Function

End Class
