﻿Public Class UserInfo

    ''' <summary>
    ''' ログイン画面からのユーザー。
    ''' </summary>
    Private _loginUser As UserRow

    ''' <summary>
    ''' 基本認証ユーザー。
    ''' </summary>
    Public ReadOnly Property BasicUser As UserRow

    ''' <summary>
    ''' ログイン画面からのユーザー。
    ''' </summary>
    Public Property LoginUser() As UserRow
        Get
            Return _loginUser
        End Get
        Private Set
            _loginUser = Value
        End Set
    End Property

    ''' <summary>
    ''' Initializes a new instance of the <see cref="UserInfo"/> class.
    ''' </summary>
    ''' <param name="basicUser">basicUser</param>
    ''' <param name="loginUser">loginUser</param>
    Public Sub New(ByVal basicUser As UserRow, ByVal loginUser As UserRow)
        Me.BasicUser = basicUser
        Me.LoginUser = loginUser
    End Sub

    ''' <summary>
    ''' ログインユーザーを変更する。
    ''' </summary>
    ''' <param name="loginUser">ユーザー情報。</param>
    Public Sub ChangeLoginUser(ByVal loginUser As UserRow)
        Me.LoginUser = loginUser
    End Sub
End Class
