﻿Imports System.Data.SqlClient

Public Class SqlCommandManager
    Implements IDisposable

#Region "メンバ"

    ''' <summary>
    ''' SqlCommandを保持するフィールド。
    ''' </summary>
    Private _command As SqlCommand

#End Region

    ''' <summary>
    ''' コンストラクタ
    ''' </summary>
    Public Sub New()
        Me._command = New SqlCommand()
        Me._command.CommandTimeout = 60
    End Sub

    ''' <summary>
    ''' コンストラクタ
    ''' </summary>
    ''' <param name="commandText">実行SQL文字列。</param>
    Public Sub New(commandText As String)
        Me._command = New SqlCommand(commandText)
    End Sub

    ''' <summary>
    ''' 実行SQLを設定する。
    ''' </summary>
    ''' <param name="commandText">実行SQL文字列。</param>
    Public Sub SetCommandText(commandText As String)
        Me._command.CommandText = commandText
    End Sub

    ''' <summary>
    ''' 実行SQL解釈タイプを設定する。
    ''' </summary>
    ''' <param name="commandType">実行SQL解釈タイプ。</param>
    Public Sub SetCommandType(commandType As CommandType)
        Me._command.CommandType = commandType
    End Sub

    ''' <summary>
    ''' 待機時間を設定する。
    ''' </summary>
    ''' <param name="time">待機時間。</param>
    Public Sub SetTimeOut(time As Integer)
        Me._command.CommandTimeout = time
    End Sub

    ''' <summary>
    ''' SQLCommandParameterを設定する。
    ''' </summary>
    ''' <param name="parameterName">パラメータ名。</param>
    ''' <param name="value">パラメータ値。</param>
    Public Sub SetParameterValue(parameterName As String, value As Object)
        Me._command.Parameters(parameterName).Value = value
    End Sub

    ''' <summary>
    '''  SQLCommandParameterを設定する。
    ''' </summary>
    ''' <param name="parameterName">パラメータ名。</param>
    ''' <param name="sqlDbType">パラメータ型。</param>
    Public Sub AddParameter(parameterName As String, sqlDbType As SqlDbType)
        Me._command.Parameters.Add(parameterName, sqlDbType)
    End Sub

    ''' <summary>
    '''  SQLCommandParameterを設定する。
    ''' </summary>
    ''' <param name="parameterName">パラメータ名。</param>
    ''' <param name="sqlDbType">パラメータ型。</param>
    ''' <param name="value">パラメータ値。</param>
    Public Sub AddParameter(parameterName As String, sqlDbType As SqlDbType, value As Object)
        Me._command.Parameters.Add(parameterName, sqlDbType)
        Me.SetParameterValue(parameterName, value)
    End Sub

    ''' <summary>
    '''  SQLCommandParameterを設定する。
    ''' </summary>
    ''' <param name="parameterName">パラメータ名。</param>
    ''' <param name="sqlDbType">パラメータ型。</param>
    ''' <param name="size">パラメータサイズ。</param>
    ''' <param name="value">パラメータ値。</param>
    Public Sub AddParameter(parameterName As String, sqlDbType As SqlDbType, size As Integer, value As Object)
        Me._command.Parameters.Add(parameterName, sqlDbType, size)
        Me.SetParameterValue(parameterName, value)
    End Sub

    ''' <summary>
    '''  SQLCommandOutPutParameterを設定する。
    ''' </summary>
    ''' <param name="parameterName">パラメータ名。</param>
    ''' <param name="sqlDbType">パラメータ型。</param>
    Public Sub AddOutPutParameter(parameterName As String, sqlDbType As SqlDbType)
        Dim parameter = Me._command.Parameters.Add(parameterName, sqlDbType)
        parameter.Direction = ParameterDirection.Output
    End Sub

    ''' <summary>
    '''  SQLCommandOutPutParameterを設定する。
    ''' </summary>
    ''' <param name="parameterName">パラメータ名。</param>
    ''' <param name="sqlDbType">パラメータ型。</param>
    ''' <param name="size">パラメータサイズ。</param>
    Public Sub AddOutPutParameter(parameterName As String, sqlDbType As SqlDbType, size As Integer)
        Dim parameter = Me._command.Parameters.Add(parameterName, sqlDbType, size)
        parameter.Direction = ParameterDirection.Output
    End Sub

    ''' <summary>
    ''' SQLCommandOutPutParameterを設定する。
    ''' </summary>
    ''' <param name="parameterName">パラメータ名。</param>
    ''' <param name="sqlDbType">パラメータ型。</param>
    ''' <param name="precision">総桁数。</param>
    ''' <param name="scale">小数点以下の桁数。</param>
    Public Sub AddOutPutParameter(parameterName As String, sqlDbType As SqlDbType, precision As Byte, scale As Byte)
        Dim parameter = Me._command.Parameters.Add(parameterName, sqlDbType)
        parameter.Precision = precision
        parameter.Scale = scale
        parameter.Direction = ParameterDirection.Output
    End Sub

    ''' <summary>
    ''' OutPutParameterを取得する。
    ''' </summary>
    ''' <param name="parameterName">パラメータ名。</param>
    ''' <returns>OutPutParameter。</returns>
    Public Function GetOutPutValue(parameterName As String) As Object
        Return Me._command.Parameters(parameterName).Value
    End Function

    ''' <summary>
    ''' SqlCommandオブジェクトを取得する。
    ''' </summary>
    ''' <returns>SqlCommandオブジェクト。</returns>
    Public Function GetCommand() As SqlCommand
        Return Me._command
    End Function

    ''' <summary>
    ''' SqlCommandオブジェクトで設定したパラメータ情報を取得する。
    ''' </summary>
    ''' <returns>パラメータ情報。</returns>
    Public Function GetCommandContents() As String

        Dim contents = String.Empty
        contents += "CommandText:" + Me._command.CommandText + vbCrLf
        contents += "CommandTimeout:" + Me._command.CommandTimeout.ToString() + vbCrLf
        contents += "CommandType:" + Me._command.CommandType.ToString() + vbCrLf
        Dim i As Integer
        For i = 0 To Me._command.Parameters.Count - 1
            contents += "---------------" + vbCrLf
            contents += "ParameterName:" + Me._command.Parameters(i).ParameterName + vbCrLf
            contents += "DbType:" + Me._command.Parameters(i).DbType.ToString() + vbCrLf
            contents += "Size:" + Me._command.Parameters(i).Size.ToString() + vbCrLf
            contents += "Precision:" + Me._command.Parameters(i).Precision.ToString() + vbCrLf
            contents += "Scale:" + Me._command.Parameters(i).Scale.ToString() + vbCrLf
            contents += "Value:" + Me._command.Parameters(i).Value.ToString() + vbCrLf
            contents += "---------------" + vbCrLf
        Next i
        Return contents
    End Function


#Region "IDisposable Support"
    Private disposedValue As Boolean ' 重複する呼び出しを検出するには

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                If Me._command IsNot Nothing Then
                    Me._command.Dispose()
                    Me._command = Nothing
                End If
            End If
        End If
        disposedValue = True
    End Sub

    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
