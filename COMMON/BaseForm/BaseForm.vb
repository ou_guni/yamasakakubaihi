﻿Public Class BaseForm
    Private buttons As System.Windows.Forms.Button() = New System.Windows.Forms.Button(13) {}   'ボタン可視設定用のコントロール配列
    Private labels As System.Windows.Forms.Label() = New System.Windows.Forms.Label(13) {}      'ラベル可視設定用のコントロール配列
    Private labelTitle As System.Windows.Forms.Label = New System.Windows.Forms.Label()         'フォームタイトル設定用のコントロール変数
    Private i As Integer = 0
    Private str As String = ""

    Private Sub BaseForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try

            If (Me.DesignMode) Then Return

            For i = 1 To 12
                str = String.Concat("BTN_", i.ToString().PadLeft(2, "0"))
                buttons(i) = CType(Me.Controls(str), System.Windows.Forms.Button)
            Next

            For i = 1 To 12
                str = String.Concat("LBL_F", i.ToString().PadLeft(2, "0"))
                labels(i) = CType(Me.Controls(str), System.Windows.Forms.Label)
            Next

            For i = 1 To 12
                buttons(i).TabStop = False
            Next

            labelTitle = Me.LBL_TITLE

            Me.BTN_12.CausesValidation = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'ファンクションキーイベント
    Private Sub BaseForm_KeyDown(sender As Object, e As Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            Select Case e.KeyCode.ToString()
                Case "F1"
                    If Me.BTN_01.Visible = True And Me.BTN_01.Enabled = True Then
                        Me.BTN_01.Select()
                        Me.BTN_01.PerformClick()
                    End If
                Case "F2"
                    If Me.BTN_02.Visible = True And Me.BTN_02.Enabled = True Then
                        Me.BTN_02.Select()
                        Me.BTN_02.PerformClick()
                    End If
                Case "F3"
                    If Me.BTN_03.Visible = True And Me.BTN_03.Enabled = True Then
                        Me.BTN_03.Select()
                        Me.BTN_03.PerformClick()
                    End If
                Case "F4"
                    If Me.BTN_04.Visible = True And Me.BTN_04.Enabled = True Then
                        Me.BTN_04.Select()
                        Me.BTN_04.PerformClick()
                    End If
                Case "F5"
                    If Me.BTN_05.Visible = True And Me.BTN_05.Enabled = True Then
                        Me.BTN_05.Select()
                        Me.BTN_05.PerformClick()
                    End If
                Case "F6"
                    If Me.BTN_06.Visible = True And Me.BTN_06.Enabled = True Then
                        Me.BTN_06.Select()
                        Me.BTN_06.PerformClick()
                    End If
                Case "F7"
                    If Me.BTN_07.Visible = True And Me.BTN_07.Enabled = True Then
                        Me.BTN_07.Select()
                        Me.BTN_07.PerformClick()
                    End If
                Case "F8"
                    If Me.BTN_08.Visible = True And Me.BTN_08.Enabled = True Then
                        Me.BTN_08.Select()
                        Me.BTN_08.PerformClick()
                    End If
                Case "F9"
                    If Me.BTN_09.Visible = True And Me.BTN_09.Enabled = True Then
                        Me.BTN_09.Select()
                        Me.BTN_09.PerformClick()
                    End If
                Case "F10"
                    If Me.BTN_10.Visible = True And Me.BTN_10.Enabled = True Then
                        Me.BTN_10.Select()
                        Me.BTN_10.PerformClick()
                    End If
                Case "F11"
                    If Me.BTN_11.Visible = True And Me.BTN_11.Enabled = True Then
                        Me.BTN_11.Select()
                        Me.BTN_11.PerformClick()
                    End If
                Case "F12"
                    If Me.BTN_12.Visible = True And Me.BTN_12.Enabled = True Then
                        Me.BTN_12.Select()
                        Me.BTN_12.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Overridable Sub BTN_01_Click(sender As Object, e As EventArgs) Handles BTN_01.Click

    End Sub

    Protected Overridable Sub BTN_02_Click(sender As Object, e As EventArgs) Handles BTN_02.Click

    End Sub

    Protected Overridable Sub BTN_03_Click(sender As Object, e As EventArgs) Handles BTN_03.Click

    End Sub

    Protected Overridable Sub BTN_04_Click(sender As Object, e As EventArgs) Handles BTN_04.Click

    End Sub

    Protected Overridable Sub BTN_05_Click(sender As Object, e As EventArgs) Handles BTN_05.Click

    End Sub

    Protected Overridable Sub BTN_06_Click(sender As Object, e As EventArgs) Handles BTN_06.Click

    End Sub

    Protected Overridable Sub BTN_07_Click(sender As Object, e As EventArgs) Handles BTN_07.Click

    End Sub

    Protected Overridable Sub BTN_08_Click(sender As Object, e As EventArgs) Handles BTN_08.Click

    End Sub

    Protected Overridable Sub BTN_09_Click(sender As Object, e As EventArgs) Handles BTN_09.Click

    End Sub

    Protected Overridable Sub BTN_10_Click(sender As Object, e As EventArgs) Handles BTN_10.Click

    End Sub

    Protected Overridable Sub BTN_11_Click(sender As Object, e As EventArgs) Handles BTN_11.Click

    End Sub

    Protected Overridable Sub BTN_12_Click(sender As Object, e As EventArgs) Handles BTN_12.Click

    End Sub

    ''' <summary>
    ''' ファンクションボタン設定 (継承フォームから実行される)
    ''' </summary>
    ''' <param name="FKey_Index">Index（1～12）</param>
    ''' <param name="wFKeyName">Text</param>
    ''' <param name="wColor">ForeColor</param>
    ''' <param name="wFont">FontSize</param>
    ''' <param name="wFontStyle">FontStyle</param>
    Protected Sub ctlFKey(FKey_Index As Byte,
                       wFKeyName As String,
                       wColor As System.Drawing.Color,
                       wFont As Byte,
                       wFontStyle As System.Drawing.FontStyle)
        Try
            If wFont > 0 Then
                labels(FKey_Index).Visible = True
                buttons(FKey_Index).Visible = True
                buttons(FKey_Index).Text = wFKeyName
                buttons(FKey_Index).Font = New System.Drawing.Font("Meiryo UI", wFont, wFontStyle)
                buttons(FKey_Index).ForeColor = wColor
            Else
                labels(FKey_Index).Visible = False
                buttons(FKey_Index).Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' ファンクションボタンロック (継承フォームから実行される)
    ''' </summary>
    ''' <param name="FKey_Index">Index（1～12）</param>
    Protected Sub btnLock(FKey_Index As Byte)
        Try
            '２重実行防止などで利用
            buttons(FKey_Index).Enabled = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' ファンクションボタンロック解除 (継承フォームから実行される)
    ''' </summary>
    ''' <param name="FKey_Index">Index（1～12）</param>
    Protected Sub btnUnLock(FKey_Index As Byte)
        Try
            buttons(FKey_Index).Enabled = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' フォームタイトルの設定 (継承フォームから実行される)
    ''' </summary>
    ''' <param name="pTitle">Formタイトル</param>
    Protected Sub setTitle(pTitle As String)
        Try
            labelTitle.Text = pTitle
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' フォームタイトルの設定 (継承フォームから実行される)
    ''' </summary>
    ''' <param name="pTitle">Formタイトル</param>
    ''' <param name="pColor">BackColor</param>
    Protected Sub setTitle(pTitle As String, pColor As System.Drawing.Color)
        Try
            labelTitle.Text = pTitle
            labelTitle.BackColor = pColor
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class