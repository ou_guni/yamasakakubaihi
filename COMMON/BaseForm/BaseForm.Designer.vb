﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BaseForm
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LBL_TITLE = New System.Windows.Forms.Label()
        Me.LBL_BTN = New System.Windows.Forms.Label()
        Me.LBL_F01 = New System.Windows.Forms.Label()
        Me.LBL_F02 = New System.Windows.Forms.Label()
        Me.LBL_F03 = New System.Windows.Forms.Label()
        Me.LBL_F04 = New System.Windows.Forms.Label()
        Me.LBL_F05 = New System.Windows.Forms.Label()
        Me.LBL_F06 = New System.Windows.Forms.Label()
        Me.LBL_F07 = New System.Windows.Forms.Label()
        Me.LBL_F08 = New System.Windows.Forms.Label()
        Me.LBL_F09 = New System.Windows.Forms.Label()
        Me.LBL_F10 = New System.Windows.Forms.Label()
        Me.LBL_F11 = New System.Windows.Forms.Label()
        Me.LBL_F12 = New System.Windows.Forms.Label()
        Me.BTN_01 = New System.Windows.Forms.Button()
        Me.BTN_02 = New System.Windows.Forms.Button()
        Me.BTN_03 = New System.Windows.Forms.Button()
        Me.BTN_04 = New System.Windows.Forms.Button()
        Me.BTN_05 = New System.Windows.Forms.Button()
        Me.BTN_06 = New System.Windows.Forms.Button()
        Me.BTN_07 = New System.Windows.Forms.Button()
        Me.BTN_08 = New System.Windows.Forms.Button()
        Me.BTN_09 = New System.Windows.Forms.Button()
        Me.BTN_10 = New System.Windows.Forms.Button()
        Me.BTN_11 = New System.Windows.Forms.Button()
        Me.BTN_12 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LBL_TITLE
        '
        Me.LBL_TITLE.BackColor = System.Drawing.Color.DimGray
        Me.LBL_TITLE.Font = New System.Drawing.Font("Meiryo UI", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_TITLE.ForeColor = System.Drawing.Color.White
        Me.LBL_TITLE.Location = New System.Drawing.Point(0, 0)
        Me.LBL_TITLE.Name = "LBL_TITLE"
        Me.LBL_TITLE.Size = New System.Drawing.Size(1182, 40)
        Me.LBL_TITLE.TabIndex = 0
        Me.LBL_TITLE.Text = "Label1"
        Me.LBL_TITLE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_BTN
        '
        Me.LBL_BTN.BackColor = System.Drawing.Color.Navy
        Me.LBL_BTN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LBL_BTN.Location = New System.Drawing.Point(8, 704)
        Me.LBL_BTN.Name = "LBL_BTN"
        Me.LBL_BTN.Size = New System.Drawing.Size(1166, 80)
        Me.LBL_BTN.TabIndex = 1
        '
        'LBL_F01
        '
        Me.LBL_F01.BackColor = System.Drawing.Color.Navy
        Me.LBL_F01.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_F01.ForeColor = System.Drawing.Color.White
        Me.LBL_F01.Location = New System.Drawing.Point(16, 764)
        Me.LBL_F01.Name = "LBL_F01"
        Me.LBL_F01.Size = New System.Drawing.Size(94, 20)
        Me.LBL_F01.TabIndex = 2
        Me.LBL_F01.Text = "F1"
        Me.LBL_F01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_F02
        '
        Me.LBL_F02.BackColor = System.Drawing.Color.Navy
        Me.LBL_F02.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_F02.ForeColor = System.Drawing.Color.White
        Me.LBL_F02.Location = New System.Drawing.Point(110, 764)
        Me.LBL_F02.Name = "LBL_F02"
        Me.LBL_F02.Size = New System.Drawing.Size(94, 20)
        Me.LBL_F02.TabIndex = 3
        Me.LBL_F02.Text = "F2"
        Me.LBL_F02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_F03
        '
        Me.LBL_F03.BackColor = System.Drawing.Color.Navy
        Me.LBL_F03.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_F03.ForeColor = System.Drawing.Color.White
        Me.LBL_F03.Location = New System.Drawing.Point(204, 764)
        Me.LBL_F03.Name = "LBL_F03"
        Me.LBL_F03.Size = New System.Drawing.Size(94, 20)
        Me.LBL_F03.TabIndex = 4
        Me.LBL_F03.Text = "F3"
        Me.LBL_F03.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_F04
        '
        Me.LBL_F04.BackColor = System.Drawing.Color.Navy
        Me.LBL_F04.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_F04.ForeColor = System.Drawing.Color.White
        Me.LBL_F04.Location = New System.Drawing.Point(298, 764)
        Me.LBL_F04.Name = "LBL_F04"
        Me.LBL_F04.Size = New System.Drawing.Size(94, 20)
        Me.LBL_F04.TabIndex = 5
        Me.LBL_F04.Text = "F4"
        Me.LBL_F04.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_F05
        '
        Me.LBL_F05.BackColor = System.Drawing.Color.Navy
        Me.LBL_F05.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_F05.ForeColor = System.Drawing.Color.White
        Me.LBL_F05.Location = New System.Drawing.Point(404, 764)
        Me.LBL_F05.Name = "LBL_F05"
        Me.LBL_F05.Size = New System.Drawing.Size(94, 20)
        Me.LBL_F05.TabIndex = 6
        Me.LBL_F05.Text = "F5"
        Me.LBL_F05.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_F06
        '
        Me.LBL_F06.BackColor = System.Drawing.Color.Navy
        Me.LBL_F06.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_F06.ForeColor = System.Drawing.Color.White
        Me.LBL_F06.Location = New System.Drawing.Point(498, 764)
        Me.LBL_F06.Name = "LBL_F06"
        Me.LBL_F06.Size = New System.Drawing.Size(94, 20)
        Me.LBL_F06.TabIndex = 7
        Me.LBL_F06.Text = "F6"
        Me.LBL_F06.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_F07
        '
        Me.LBL_F07.BackColor = System.Drawing.Color.Navy
        Me.LBL_F07.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_F07.ForeColor = System.Drawing.Color.White
        Me.LBL_F07.Location = New System.Drawing.Point(592, 764)
        Me.LBL_F07.Name = "LBL_F07"
        Me.LBL_F07.Size = New System.Drawing.Size(94, 20)
        Me.LBL_F07.TabIndex = 8
        Me.LBL_F07.Text = "F7"
        Me.LBL_F07.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_F08
        '
        Me.LBL_F08.BackColor = System.Drawing.Color.Navy
        Me.LBL_F08.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_F08.ForeColor = System.Drawing.Color.White
        Me.LBL_F08.Location = New System.Drawing.Point(686, 764)
        Me.LBL_F08.Name = "LBL_F08"
        Me.LBL_F08.Size = New System.Drawing.Size(94, 20)
        Me.LBL_F08.TabIndex = 9
        Me.LBL_F08.Text = "F8"
        Me.LBL_F08.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_F09
        '
        Me.LBL_F09.BackColor = System.Drawing.Color.Navy
        Me.LBL_F09.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_F09.ForeColor = System.Drawing.Color.White
        Me.LBL_F09.Location = New System.Drawing.Point(792, 764)
        Me.LBL_F09.Name = "LBL_F09"
        Me.LBL_F09.Size = New System.Drawing.Size(94, 20)
        Me.LBL_F09.TabIndex = 10
        Me.LBL_F09.Text = "F9"
        Me.LBL_F09.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_F10
        '
        Me.LBL_F10.BackColor = System.Drawing.Color.Navy
        Me.LBL_F10.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_F10.ForeColor = System.Drawing.Color.White
        Me.LBL_F10.Location = New System.Drawing.Point(886, 764)
        Me.LBL_F10.Name = "LBL_F10"
        Me.LBL_F10.Size = New System.Drawing.Size(94, 20)
        Me.LBL_F10.TabIndex = 11
        Me.LBL_F10.Text = "F10"
        Me.LBL_F10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_F11
        '
        Me.LBL_F11.BackColor = System.Drawing.Color.Navy
        Me.LBL_F11.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_F11.ForeColor = System.Drawing.Color.White
        Me.LBL_F11.Location = New System.Drawing.Point(980, 764)
        Me.LBL_F11.Name = "LBL_F11"
        Me.LBL_F11.Size = New System.Drawing.Size(94, 20)
        Me.LBL_F11.TabIndex = 12
        Me.LBL_F11.Text = "F11"
        Me.LBL_F11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LBL_F12
        '
        Me.LBL_F12.BackColor = System.Drawing.Color.Navy
        Me.LBL_F12.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.LBL_F12.ForeColor = System.Drawing.Color.White
        Me.LBL_F12.Location = New System.Drawing.Point(1074, 764)
        Me.LBL_F12.Name = "LBL_F12"
        Me.LBL_F12.Size = New System.Drawing.Size(94, 20)
        Me.LBL_F12.TabIndex = 13
        Me.LBL_F12.Text = "F12"
        Me.LBL_F12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BTN_01
        '
        Me.BTN_01.Font = New System.Drawing.Font("Meiryo UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.BTN_01.Location = New System.Drawing.Point(16, 714)
        Me.BTN_01.Name = "BTN_01"
        Me.BTN_01.Size = New System.Drawing.Size(94, 50)
        Me.BTN_01.TabIndex = 14
        Me.BTN_01.Text = "Button1"
        Me.BTN_01.UseVisualStyleBackColor = True
        '
        'BTN_02
        '
        Me.BTN_02.Font = New System.Drawing.Font("Meiryo UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.BTN_02.Location = New System.Drawing.Point(110, 714)
        Me.BTN_02.Name = "BTN_02"
        Me.BTN_02.Size = New System.Drawing.Size(94, 50)
        Me.BTN_02.TabIndex = 15
        Me.BTN_02.Text = "Button2"
        Me.BTN_02.UseVisualStyleBackColor = True
        '
        'BTN_03
        '
        Me.BTN_03.Font = New System.Drawing.Font("Meiryo UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.BTN_03.Location = New System.Drawing.Point(204, 714)
        Me.BTN_03.Name = "BTN_03"
        Me.BTN_03.Size = New System.Drawing.Size(94, 50)
        Me.BTN_03.TabIndex = 16
        Me.BTN_03.Text = "Button3"
        Me.BTN_03.UseVisualStyleBackColor = True
        '
        'BTN_04
        '
        Me.BTN_04.Font = New System.Drawing.Font("Meiryo UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.BTN_04.Location = New System.Drawing.Point(298, 714)
        Me.BTN_04.Name = "BTN_04"
        Me.BTN_04.Size = New System.Drawing.Size(94, 50)
        Me.BTN_04.TabIndex = 17
        Me.BTN_04.Text = "Button4"
        Me.BTN_04.UseVisualStyleBackColor = True
        '
        'BTN_05
        '
        Me.BTN_05.Font = New System.Drawing.Font("Meiryo UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.BTN_05.Location = New System.Drawing.Point(403, 714)
        Me.BTN_05.Name = "BTN_05"
        Me.BTN_05.Size = New System.Drawing.Size(94, 50)
        Me.BTN_05.TabIndex = 18
        Me.BTN_05.Text = "Button5"
        Me.BTN_05.UseVisualStyleBackColor = True
        '
        'BTN_06
        '
        Me.BTN_06.Font = New System.Drawing.Font("Meiryo UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.BTN_06.Location = New System.Drawing.Point(497, 714)
        Me.BTN_06.Name = "BTN_06"
        Me.BTN_06.Size = New System.Drawing.Size(94, 50)
        Me.BTN_06.TabIndex = 19
        Me.BTN_06.Text = "Button6"
        Me.BTN_06.UseVisualStyleBackColor = True
        '
        'BTN_07
        '
        Me.BTN_07.Font = New System.Drawing.Font("Meiryo UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.BTN_07.Location = New System.Drawing.Point(591, 714)
        Me.BTN_07.Name = "BTN_07"
        Me.BTN_07.Size = New System.Drawing.Size(94, 50)
        Me.BTN_07.TabIndex = 20
        Me.BTN_07.Text = "Button7"
        Me.BTN_07.UseVisualStyleBackColor = True
        '
        'BTN_08
        '
        Me.BTN_08.Font = New System.Drawing.Font("Meiryo UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.BTN_08.Location = New System.Drawing.Point(685, 714)
        Me.BTN_08.Name = "BTN_08"
        Me.BTN_08.Size = New System.Drawing.Size(94, 50)
        Me.BTN_08.TabIndex = 21
        Me.BTN_08.Text = "Button8"
        Me.BTN_08.UseVisualStyleBackColor = True
        '
        'BTN_09
        '
        Me.BTN_09.Font = New System.Drawing.Font("Meiryo UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.BTN_09.Location = New System.Drawing.Point(790, 714)
        Me.BTN_09.Name = "BTN_09"
        Me.BTN_09.Size = New System.Drawing.Size(94, 50)
        Me.BTN_09.TabIndex = 22
        Me.BTN_09.Text = "Button9"
        Me.BTN_09.UseVisualStyleBackColor = True
        '
        'BTN_10
        '
        Me.BTN_10.Font = New System.Drawing.Font("Meiryo UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.BTN_10.Location = New System.Drawing.Point(884, 714)
        Me.BTN_10.Name = "BTN_10"
        Me.BTN_10.Size = New System.Drawing.Size(94, 50)
        Me.BTN_10.TabIndex = 23
        Me.BTN_10.Text = "Button10"
        Me.BTN_10.UseVisualStyleBackColor = True
        '
        'BTN_11
        '
        Me.BTN_11.Font = New System.Drawing.Font("Meiryo UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.BTN_11.Location = New System.Drawing.Point(978, 714)
        Me.BTN_11.Name = "BTN_11"
        Me.BTN_11.Size = New System.Drawing.Size(94, 50)
        Me.BTN_11.TabIndex = 24
        Me.BTN_11.Text = "Button11"
        Me.BTN_11.UseVisualStyleBackColor = True
        '
        'BTN_12
        '
        Me.BTN_12.Font = New System.Drawing.Font("Meiryo UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.BTN_12.Location = New System.Drawing.Point(1072, 714)
        Me.BTN_12.Name = "BTN_12"
        Me.BTN_12.Size = New System.Drawing.Size(94, 50)
        Me.BTN_12.TabIndex = 25
        Me.BTN_12.Text = "Button12"
        Me.BTN_12.UseVisualStyleBackColor = True
        '
        'BaseForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1182, 793)
        Me.Controls.Add(Me.BTN_12)
        Me.Controls.Add(Me.BTN_11)
        Me.Controls.Add(Me.BTN_10)
        Me.Controls.Add(Me.BTN_09)
        Me.Controls.Add(Me.BTN_08)
        Me.Controls.Add(Me.BTN_07)
        Me.Controls.Add(Me.BTN_06)
        Me.Controls.Add(Me.BTN_05)
        Me.Controls.Add(Me.BTN_04)
        Me.Controls.Add(Me.BTN_03)
        Me.Controls.Add(Me.BTN_02)
        Me.Controls.Add(Me.BTN_01)
        Me.Controls.Add(Me.LBL_F12)
        Me.Controls.Add(Me.LBL_F11)
        Me.Controls.Add(Me.LBL_F10)
        Me.Controls.Add(Me.LBL_F09)
        Me.Controls.Add(Me.LBL_F08)
        Me.Controls.Add(Me.LBL_F07)
        Me.Controls.Add(Me.LBL_F06)
        Me.Controls.Add(Me.LBL_F05)
        Me.Controls.Add(Me.LBL_F04)
        Me.Controls.Add(Me.LBL_F03)
        Me.Controls.Add(Me.LBL_F02)
        Me.Controls.Add(Me.LBL_F01)
        Me.Controls.Add(Me.LBL_BTN)
        Me.Controls.Add(Me.LBL_TITLE)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "BaseForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BaseForm"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LBL_TITLE As Windows.Forms.Label
    Friend WithEvents LBL_BTN As Windows.Forms.Label
    Friend WithEvents LBL_F01 As Windows.Forms.Label
    Friend WithEvents LBL_F02 As Windows.Forms.Label
    Friend WithEvents LBL_F03 As Windows.Forms.Label
    Friend WithEvents LBL_F04 As Windows.Forms.Label
    Friend WithEvents LBL_F05 As Windows.Forms.Label
    Friend WithEvents LBL_F06 As Windows.Forms.Label
    Friend WithEvents LBL_F07 As Windows.Forms.Label
    Friend WithEvents LBL_F08 As Windows.Forms.Label
    Friend WithEvents LBL_F09 As Windows.Forms.Label
    Friend WithEvents LBL_F10 As Windows.Forms.Label
    Friend WithEvents LBL_F11 As Windows.Forms.Label
    Friend WithEvents LBL_F12 As Windows.Forms.Label
    Friend WithEvents BTN_01 As Windows.Forms.Button
    Friend WithEvents BTN_02 As Windows.Forms.Button
    Friend WithEvents BTN_03 As Windows.Forms.Button
    Friend WithEvents BTN_04 As Windows.Forms.Button
    Friend WithEvents BTN_05 As Windows.Forms.Button
    Friend WithEvents BTN_06 As Windows.Forms.Button
    Friend WithEvents BTN_07 As Windows.Forms.Button
    Friend WithEvents BTN_08 As Windows.Forms.Button
    Friend WithEvents BTN_09 As Windows.Forms.Button
    Friend WithEvents BTN_10 As Windows.Forms.Button
    Friend WithEvents BTN_11 As Windows.Forms.Button
    Friend WithEvents BTN_12 As Windows.Forms.Button
End Class
