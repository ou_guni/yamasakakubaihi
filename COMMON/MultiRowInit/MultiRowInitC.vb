﻿Imports System.Drawing
Imports System.Windows.Forms
Imports GrapeCity.Win.MultiRow

Public Class MultiRowInitC

    ' ==================================================================================================================
    ' 以下の３ファイルへの参照設定が必要です。
    '
    ' (1)GrapeCity.Win.MultiRow.v100（グリッド コンポーネント）
    '    C:\Program Files (x86)\MultiRowWin10\Bin\v10.0.4006.2012\GrapeCity.Win.MultiRow.v100.dll
    '
    ' (2)GrapeCity.Framework.MultiRow.v24（GrapeCity 共通フレームワーク）
    '    C:\Program Files (x86)\MultiRowWin10\Bin\v10.0.4006.2012\GrapeCity.Framework.MultiRow.v24.dll
    '
    ' (3)GrapeCity.Win.Editors.v100（InputManコンポーネント）
    '    C:\develop\yamasakakubaihi\exe\GrapeCity.Win.Editors.v100.dll
    '    ※[%インストール フォルダ%]\ExternalAsm\ExternalAssembly.zipに含まれるため、解凍し上記フォルダにコピーが必要。
    ' ==================================================================================================================

#Region "■ MultiRow (mrColHeader) ■"
    ''' <summary>
    ''' MultiRow：ColumnHeaderCell共通設定
    ''' </summary>
    ''' <param name="ch">ColumnHeaderCell名</param>
    Public Sub mrColHeader(ch As GrapeCity.Win.MultiRow.ColumnHeaderCell)
        Try
            'mrColHeaderCell_01.Name = "ColumnHeaderCell1"
            'mrColHeaderCell_01.Style.Border =BorderStyle.FixedSingle
            'mrColHeaderCell_01.UseVisualStyleBackColor = False

            Dim Border1 As GrapeCity.Win.MultiRow.Border = New GrapeCity.Win.MultiRow.Border()
            Border1.Outline = New GrapeCity.Win.MultiRow.Line(GrapeCity.Win.MultiRow.LineStyle.Thin, System.Drawing.Color.Black)

            ch.Value = "No"
            ch.Style.Border = Border1
            ch.Style.BackColor = Color.AliceBlue
            ch.Style.ForeColor = Color.Black
            ch.FlatStyle = FlatStyle.Flat
            ch.Style.TextAlign = MultiRowContentAlignment.MiddleLeft
            ch.Style.Font = New Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
            ch.TabStop = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "■ MultiRow (mrFormat) ■"
    ''' <summary>
    ''' MultiRow：MultiRow共通設定
    ''' </summary>
    ''' <param name="mr">MultiRow名</param>
    Public Sub mrFormat(mr As GrapeCity.Win.MultiRow.GcMultiRow)
        Try
            mr.AllowAutoExtend = False                             ' セクションをGcMultiRowの幅に自動拡張
            mr.AllowBorderLineZoom = False                         ' ズーム機能によって境界線が拡大 / 縮小
            'mr.AllowDrop = false                                  ' ドロップ許可
            mr.AllowUserToDeleteRows = False                       ' UI操作によってGcMultiRowから行削除
            mr.AllowUserToResize = False                           ' セルサイズ変更
            mr.AllowUserToZoom = False                             ' [Ctrl]キー+マウスホイール回転でのズーム
            mr.MultiSelect = False                                 ' 複数選択許可
            mr.SplitMode = GrapeCity.Win.MultiRow.SplitMode.None   ' コントロールの分割

            ' スクロールバー
            mr.HorizontalScrollBarMode = GrapeCity.Win.MultiRow.ScrollBarMode.Automatic
            mr.HorizontalScrollMode = ScrollMode.Cell
            mr.VerticalScrollBarMode = GrapeCity.Win.MultiRow.ScrollBarMode.Automatic

            ' Enterキーでカーソル移動
            mr.ShortcutKeyManager.Unregister(Keys.Enter)
            mr.ShortcutKeyManager.Register(GrapeCity.Win.MultiRow.SelectionActions.MoveToNextCell, Keys.Enter)
            ' Escを2回押した場合に行データ削除を防止
            mr.ShortcutKeyManager.Unregister(Keys.Escape)
            mr.ShortcutKeyManager.Register(GrapeCity.Win.MultiRow.EditingActions.CancelCellEdit, Keys.Escape)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "■ MultiRow：ラベル型 (mrLabel) ■"
    ''' <summary>
    ''' MultiRow：ラベル型共通設定
    ''' </summary>
    ''' <param name="ctl">ラベルコントロール名</param>
    ''' <param name="cAlign">文字表示位置</param>
    ''' <param name="cFont">フォントサイズ</param>
    Public Sub mrLabel(ctl As GrapeCity.Win.MultiRow.LabelCell,
                       cAlign As GrapeCity.Win.MultiRow.MultiRowContentAlignment,
                       cFont As Single)
        Try
            ctl.Style.Border = New GrapeCity.Win.MultiRow.Border(GrapeCity.Win.MultiRow.LineStyle.Thin, Color.Black)
            ctl.Style.BackColor = Color.Gainsboro
            ctl.Style.Font = New System.Drawing.Font("Meiryo UI", cFont, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(128)).Clone()
            ctl.Style.TextAlign = cAlign
            ctl.Value = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "■ MultiRow：ボタン型 (mrButton) ■"
    ''' <summary>
    ''' MultiRow：ボタン型共通設定
    ''' </summary>
    ''' <param name="ctl">ボタンコントロール名</param>
    ''' <param name="cFont">フォントサイズ</param>
    Public Sub mrButton(ctl As GrapeCity.Win.MultiRow.ButtonCell,
                        cFont As Single)
        Try
            ctl.Style.Font = New System.Drawing.Font("Meiryo UI", cFont, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(128)).Clone()
            ctl.TabStop = False
            ctl.Value = "..."
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "■ MultiRow.InputMan：数値型 (mrNumber) ■"
    ''' <summary>
    ''' MultiRow：InputMan数値型共通設定
    ''' </summary>
    ''' <param name="ctl">数値コントロール名</param>
    ''' <param name="cFormat">数値書式</param>
    ''' <param name="cDispZero">ゼロの書式（未使用）</param>
    ''' <param name="cPPrefix">正の値の接頭語</param>
    ''' <param name="cMPrefix">負の値の接頭語</param>
    ''' <param name="cAlign">文字表示位置</param>
    ''' <param name="cFont">フォントサイズ</param>
    Public Sub mrNumber(ctl As GrapeCity.Win.MultiRow.InputMan.GcNumberCell,
                        cMaxValue As Decimal,
                        cMinValue As Decimal,
                        cFormat As String,
                        cDispZero As String,
                        cPPrefix As String,
                        cMPrefix As String,
                        cAlign As GrapeCity.Win.MultiRow.MultiRowContentAlignment,
                        cFont As Single)
        Try
            ctl.Style.Border = New GrapeCity.Win.MultiRow.Border(GrapeCity.Win.MultiRow.LineStyle.Thin, Color.Black)
            ctl.Style.DisabledBackColor = SystemColors.Control
            ctl.Style.DisabledForeColor = Color.Black
            ctl.Style.Font = New System.Drawing.Font("Meiryo UI", cFont, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(128)).Clone()
            ctl.Style.TextAlign = cAlign
            ctl.EditMode = GrapeCity.Win.Editors.EditMode.Overwrite
            ctl.HighlightText = True
            ctl.Style.ImeMode = System.Windows.Forms.ImeMode.Off
            ctl.MaxValue = cMaxValue
            ctl.MinValue = cMinValue
            ctl.DisplayFields.Clear()
            ctl.SideButtons.Clear()
            ctl.AlternateText.DisplayZero.Text = cDispZero
            ctl.AlternateText.DisplayNull.Text = " "
            'ctl.AlternateText.Zero.Text = cDispZero
            ctl.AlternateText.Null.Text = " "
            ctl.Fields.SetFields(cFormat, cPPrefix, "", cMPrefix, "")
            ctl.DropDown.AllowDrop = False
            ctl.Spin.AllowSpin = False
            ctl.Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "■ MultiRow.InputMan：テキスト型 (mrText) ■"
    ''' <summary>
    ''' MultiRow：InputManテキスト型共通設定
    ''' </summary>
    ''' <param name="ctl">テキストコントロール名</param>
    ''' <param name="cMaxlength">最大入力バイト数</param>
    ''' <param name="cIme">imeモード</param>
    ''' <param name="cAlign">文字表示位置</param>
    ''' <param name="cFont">フォントサイズ</param>
    Public Sub mrText(ctl As GrapeCity.Win.MultiRow.InputMan.GcTextBoxCell,
                      cMaxlength As Integer,
                      cIme As System.Windows.Forms.ImeMode,
                      cAlign As GrapeCity.Win.MultiRow.MultiRowContentAlignment,
                      cFont As Single)
        Try
            ctl.Style.Border = New GrapeCity.Win.MultiRow.Border(GrapeCity.Win.MultiRow.LineStyle.Thin, Color.Black)
            ctl.Style.DisabledBackColor = SystemColors.Control
            ctl.Style.DisabledForeColor = Color.Black
            ctl.Style.Font = New System.Drawing.Font("Meiryo UI", cFont, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(128)).Clone()
            ctl.Style.TextAlign = cAlign
            ctl.HighlightText = True
            ctl.Style.ImeMode = cIme
            ctl.MaxLengthUnit = GrapeCity.Win.Editors.LengthUnit.Byte
            ctl.MaxLength = cMaxlength
            ctl.Value = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "■ MultiRow.InputMan：マスク型 (mrMask) ■"
    ''' <summary>
    ''' MultiRow：InputManマスク型共通設定
    ''' </summary>
    ''' <param name="ctl">マスクコントロール名</param>
    ''' <param name="cMask">書式</param>
    ''' <param name="cAlign">文字表示位置</param>
    ''' <param name="cFont">フォントサイズ</param>
    Public Sub mrMask(ctl As GrapeCity.Win.MultiRow.InputMan.GcMaskCell,
                      cMask As String,
                      cAlign As GrapeCity.Win.MultiRow.MultiRowContentAlignment,
                      cFont As Single,
                      Optional pChar As Char = "_")
        Try
            ctl.Style.Border = New GrapeCity.Win.MultiRow.Border(GrapeCity.Win.MultiRow.LineStyle.Thin, Color.Black)
            ctl.Style.DisabledBackColor = SystemColors.Control
            ctl.Style.DisabledForeColor = Color.Black
            ctl.Style.Font = New System.Drawing.Font("Meiryo UI", cFont, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(128)).Clone()
            ctl.Style.ImeMode = System.Windows.Forms.ImeMode.Off
            ctl.Style.TextAlign = cAlign
            ctl.Fields.AddRange(cMask)
            ctl.HighlightText = GrapeCity.Win.Editors.HighlightText.Field
            ctl.PromptChar = pChar
            ctl.Value = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "■ MultiRow.InputMan：コンボボックス型 (mrCombo) ■"
    ''' <summary>
    ''' MultiRow：InputManコンボボックス型共通設定
    ''' </summary>
    ''' <param name="ctl">コンボボックスコントロール名</param>
    ''' <param name="cFont">フォントサイズ</param>
    ''' <param name="cWidth">コントロール幅</param>
    Public Sub mrCombo(ctl As GrapeCity.Win.MultiRow.InputMan.GcComboBoxCell,
                       cFont As Single,
                       cWidth As Integer)
        Try
            ctl.Style.Border = New GrapeCity.Win.MultiRow.Border(GrapeCity.Win.MultiRow.LineStyle.Thin, Color.Black)
            ctl.Style.DisabledBackColor = SystemColors.Control
            ctl.Style.DisabledForeColor = Color.Black
            ctl.DropDownStyle = GrapeCity.Win.MultiRow.MultiRowComboBoxStyle.DropDownList
            ctl.FlatStyle = FlatStyle.Popup
            ctl.Style.Font = New System.Drawing.Font("Meiryo UI", cFont, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(128)).Clone()
            ctl.Style.ImeMode = System.Windows.Forms.ImeMode.Off

            ctl.ListDefaultColumn.Header.AllowResize = False
            ctl.ListDefaultColumn.AutoWidth = False
            ctl.ListColumns.Clear()
            ctl.ListColumns.AddRange(New GrapeCity.Win.MultiRow.InputMan.ListColumn() {New GrapeCity.Win.MultiRow.InputMan.ListColumn("コード"),
                                                                                           New GrapeCity.Win.MultiRow.InputMan.ListColumn("名称")})
            ctl.ListHeaderPane.Visible = False
            ctl.ListColumns(0).Width = 0
            ctl.ListColumns(1).Width = cWidth
            ctl.DropDown.Width = cWidth
            ctl.DropDown.AllowResize = False
            'ctl.DropDown.AllowDrop = false
            ctl.MaxDropDownItems = 14
            ctl.Spin.AllowSpin = False
            ctl.TextSubItemIndex = 1
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "■ MultiRow.InputMan：日付型 (mrDate) ■"
    ''' <summary>
    ''' MultiRow：InputMan日付型共通設定
    ''' </summary>
    ''' <param name="ctl">日付コントロール名</param>
    ''' <param name="cFont">フォントサイズ</param>
    Public Sub mrDate(ctl As GrapeCity.Win.MultiRow.InputMan.GcDateTimeCell,
                      cFont As Single)
        Try
            ctl.Style.Border = New GrapeCity.Win.MultiRow.Border(GrapeCity.Win.MultiRow.LineStyle.Thin, Color.Black)
            ctl.Style.TextAlign = GrapeCity.Win.MultiRow.MultiRowContentAlignment.MiddleCenter
            ctl.Style.DisabledBackColor = SystemColors.Control
            ctl.Style.DisabledForeColor = Color.Black
            ctl.Fields.Clear()
            ctl.Fields.AddRange("yyyy/MM/dd")
            ctl.DisplayFields.Clear()
            ctl.DisplayFields.AddRange("yyyy/MM/dd")
            ctl.DropDown.AllowDrop = True
            ctl.Spin.AllowSpin = False
            ctl.Style.Font = New System.Drawing.Font("Meiryo UI", cFont, FontStyle.Regular, GraphicsUnit.Point, ((128)))
            ctl.HighlightText = GrapeCity.Win.Editors.HighlightText.Field
            ctl.Style.ImeMode = System.Windows.Forms.ImeMode.Off

            ctl.DropDownCalendar.Weekdays.Sunday.SubStyle.ForeColor = Color.Red
            ctl.DropDownCalendar.Weekdays.Sunday.WeekFlags = GrapeCity.Win.Editors.WeekFlags.All
            ctl.DropDownCalendar.Weekdays.Sunday.ReflectToTitle = GrapeCity.Win.Editors.ReflectTitle.ForeColor
            ctl.DropDownCalendar.Weekdays.Saturday.SubStyle.ForeColor = Color.Blue
            ctl.DropDownCalendar.Weekdays.Saturday.WeekFlags = GrapeCity.Win.Editors.WeekFlags.All
            ctl.DropDownCalendar.Weekdays.Saturday.ReflectToTitle = GrapeCity.Win.Editors.ReflectTitle.ForeColor
            ctl.DropDownCalendar.TodayImage = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "■ MultiRow：チェックボックス型 (mrCheckBox) ■"
    ''' <summary>
    ''' MultiRow：チェックボックス型共通設定
    ''' </summary>
    ''' <param name="ctl">チェックボックスコントロール名</param>
    ''' <param name="cAlign">表示位置</param>
    Public Sub mrCheckBox(ctl As GrapeCity.Win.MultiRow.CheckBoxCell,
                          cAlign As ContentAlignment)
        Try
            ctl.CheckAlign = cAlign
            ctl.Style.Border = New Border(LineStyle.Thin, Color.Black)
            ctl.TrueValue = "1"
            ctl.FalseValue = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "■ MultiRow：カラムヘッダー (mrClmHeader) ■"
    ''' <summary>
    ''' MultiRow：カラムヘッダー型共通設定
    ''' </summary>
    ''' <param name="ctl">カラムヘッダーコントロール名</param>
    ''' <param name="cAlign">文字表示位置</param>
    ''' <param name="cFont">フォントサイズ</param>
    Public Sub mrClmHeader(ctl As GrapeCity.Win.MultiRow.ColumnHeaderCell,
                           cAlign As GrapeCity.Win.MultiRow.MultiRowContentAlignment,
                           cFont As Single)
        Try
            ctl.FlatStyle = FlatStyle.Flat
            ctl.Style.BackColor = Color.Green
            ctl.Style.ForeColor = Color.White
            ctl.Style.Border = New GrapeCity.Win.MultiRow.Border(GrapeCity.Win.MultiRow.LineStyle.Thin, Color.White)
            ctl.Style.Font = New Font("Meiryo UI", cFont, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(128)).Clone()
            ctl.Style.TextAlign = cAlign
            ctl.Value = ""
            ctl.Selectable = False
            ctl.SelectionMode = GrapeCity.Win.MultiRow.MultiRowSelectionMode.None
            ctl.TabStop = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "■ MultiRow：Template (mrTmpFormat) ■"
    ''' <summary>
    ''' MultiRow：Template共通設定
    ''' </summary>
    ''' <param name="mrTmp">Template名</param>
    ''' <param name="cColCnt">列数</param>
    ''' <param name="cRowHeight">行高さ</param>
    Public Sub mrTmpFormat(mrTmp As GrapeCity.Win.MultiRow.Template,
                           cColCnt As Integer,
                           cRowHeight As Integer)
        Try
            mrTmp.ColumnHeadersDefaultHeaderCellStyle.BackColor = Color.DarkGreen
            mrTmp.ColumnHeadersDefaultHeaderCellStyle.ForeColor = Color.White
            mrTmp.ColumnHeadersDefaultHeaderCellStyle.TextAlign = GrapeCity.Win.MultiRow.MultiRowContentAlignment.MiddleCenter

            'GrapeCity.Win.MultiRow.Border headerCellBorder = New GrapeCity.Win.MultiRow.Border(GrapeCity.Win.MultiRow.LineStyle.Thin, Color.Silver)
            'Font headerCellFont = New System.Drawing.Font("ＭＳ ゴシック", 9, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(128)).Clone()
            Dim headerCellBorder As GrapeCity.Win.MultiRow.Border = New GrapeCity.Win.MultiRow.Border(GrapeCity.Win.MultiRow.LineStyle.Thin, Color.Silver)
            Dim headerCellFont As Font = New System.Drawing.Font("Meiryo UI", 9, FontStyle.Regular, GraphicsUnit.Point, Convert.ToByte(128)).Clone()


            For Each c As GrapeCity.Win.MultiRow.HeaderCell In mrTmp.ColumnHeaders(0).Cells
                ' フラット表示
                c.FlatStyle = FlatStyle.Flat
                c.Size = New Size(c.Width, mrTmp.ColumnHeaders(0).Height)   ' 列ヘッダの各セルの高さ

                ' フォントと罫線の設定（コーナーヘッダを除く）
                'If !(c Is GrapeCity.Win.MultiRow.CornerHeaderCell) Then
                'If c IsNot GrapeCity.Win.MultiRow.CornerHeaderCell Then
                '    c.Style.Font = headerCellFont
                '    c.Style.Border = headerCellBorder
                '    c.Style.Multiline = GrapeCity.Win.MultiRow.MultiRowTriState.True
                'End If
            Next

            ' レコードセレクタの非表示
            ' 行ヘッダの削除とセルの移動
            Dim offset As Integer = mrTmp.Row.Cells(cColCnt).Width
            mrTmp.Row.Cells.RemoveAt(cColCnt)
            For Each c As GrapeCity.Win.MultiRow.Cell In mrTmp.Row.Cells
                c.Location = New Point(c.Left - offset, c.Top)
            Next

            ' コーナーヘッダの削除と列ヘッダセルの移動
            mrTmp.ColumnHeaders(0).Cells.RemoveAt(mrTmp.ColumnHeaders(0).Cells.Count - 1)
            For Each c As GrapeCity.Win.MultiRow.Cell In mrTmp.ColumnHeaders(0).Cells
                c.Location = New Point(c.Left - offset, c.Top)
            Next

            ' テンプレートの幅の縮小
            mrTmp.Width -= offset


            ' 行とセルの高さの設定
            mrTmp.Row.Height = cRowHeight
            For Each c As GrapeCity.Win.MultiRow.Cell In mrTmp.Row.Cells
                c.Size = New Size(c.Width, cRowHeight)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

End Class
