﻿Public Class LoginF1

    Private Sub btn_ok_Click(sender As Object, e As EventArgs) Handles btn_ok.Click
        Try

            Dim user As Common.UserHelper = New Common.UserHelper
            ' 入力チェック
            If Len(txt_user.Text.Trim) = 0 Or Len(txt_password.Text.Trim) = 0 Then
                MessageBox.Show("ログイン名またはパスワードを入力してください。", Common.Common.GetCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                If Len(txt_user.Text.Trim) = 0 Then
                    txt_user.Select()
                Else
                    txt_password.Select()
                End If
                Return
            End If
            ' ユーザーIDとパスワードで認証
            Dim login As Boolean = user.Login(txt_user.Text.Trim, txt_password.Text.Trim)
            If login Then
                ' ユーザー情報をinitファイルに書き込み
                Dim res As Boolean = user.SetUserInfo(user.GetuserInfo)
                If Not res Then
                    Common.LogHelper.WriteLogAndShowMsg(New Exception("ユーザー情報書きこみエラー"), "ユーザー情報をシステム初期化initファイルに生成失敗しました。")
                Else
                    ' ログイン情報クリア
                    txt_user.Text = String.Empty
                    txt_password.Text = String.Empty
                    txt_user.Select()
                    'ログインフォームを非表示に
                    Me.Visible = False
                    ' メニュー画面起動
                    Dim loginF2 As New LoginF2
                    loginF2.ShowDialog()
                    If loginF2 IsNot Nothing Then
                        loginF2.Dispose()
                        loginF2 = Nothing
                    End If
                    ' ログイン画面クローズ
                    Me.Close()
                End If
            Else
                'ログ＆メッセージ
                Common.LogHelper.WriteLogAndShowMsg(New Exception("担当者コード、または、パスワードを正しく入力して下さい。。"))
            End If
        Catch ex As Exception
            '共通例外処理
            Common.LogHelper.WriteLogAndShowMsg(ex)
        End Try
    End Sub
#Region "キャンセル押下"
    ''' <summary>
    ''' キャンセル押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btn_cancel_Click(sender As Object, e As EventArgs) Handles btn_cancel.Click
        Me.Close()
    End Sub
#End Region
#Region "キーダウンイベント(Enterキー)"
    ''' <summary>
    ''' キーダウンイベント
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub LoginF1_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode.Equals(Keys.Enter) Then
                '//Enterキーの場合//
                Dim forward As Boolean = e.Modifiers <> Keys.Shift
                '次（前）コントロールへ移動
                Me.SelectNextControl(Me.ActiveControl, forward, True, True, True)
                e.Handled = True
            End If
        Catch ex As Exception
            '共通例外処理
            Common.LogHelper.WriteLogAndShowMsg(ex)
        End Try
    End Sub
    ''' <summary>
    ''' ページロード処理
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub LoginF1_Load(sender As Object, e As EventArgs) Handles Me.Load
        'フォームが先にキーを受け取る設定
        Me.KeyPreview = True
    End Sub
#End Region
End Class