﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class LoginF2
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LoginF2))
        Me.pnl_title = New System.Windows.Forms.Panel()
        Me.lbl_menu_title = New System.Windows.Forms.Label()
        Me.lnk_logout = New System.Windows.Forms.LinkLabel()
        Me.lbl_section = New System.Windows.Forms.Label()
        Me.lbl_user = New System.Windows.Forms.Label()
        Me.btn_close = New System.Windows.Forms.Button()
        Me.btn_PaymentApproval = New System.Windows.Forms.Button()
        Me.btn_AccountPayable = New System.Windows.Forms.Button()
        Me.btn_SalesPromotionStoreMaster = New System.Windows.Forms.Button()
        Me.btn_ApplicationApproval = New System.Windows.Forms.Button()
        Me.btn_PaymentCollation = New System.Windows.Forms.Button()
        Me.btn_StoreCodeConversionMaster = New System.Windows.Forms.Button()
        Me.btn_AnnualInput = New System.Windows.Forms.Button()
        Me.btn_PaymentDataOutput = New System.Windows.Forms.Button()
        Me.btn_PaymentDataCreate = New System.Windows.Forms.Button()
        Me.btn_ItemCodeConversionMaster = New System.Windows.Forms.Button()
        Me.btn_MonthlyApplication = New System.Windows.Forms.Button()
        Me.pnl_title.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnl_title
        '
        Me.pnl_title.BackColor = System.Drawing.SystemColors.HotTrack
        Me.pnl_title.Controls.Add(Me.lbl_menu_title)
        Me.pnl_title.Controls.Add(Me.lnk_logout)
        Me.pnl_title.Controls.Add(Me.lbl_section)
        Me.pnl_title.Controls.Add(Me.lbl_user)
        Me.pnl_title.Location = New System.Drawing.Point(1, 0)
        Me.pnl_title.Name = "pnl_title"
        Me.pnl_title.Size = New System.Drawing.Size(902, 40)
        Me.pnl_title.TabIndex = 7
        '
        'lbl_menu_title
        '
        Me.lbl_menu_title.AutoSize = True
        Me.lbl_menu_title.Font = New System.Drawing.Font("Meiryo UI", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lbl_menu_title.ForeColor = System.Drawing.Color.White
        Me.lbl_menu_title.Location = New System.Drawing.Point(326, 6)
        Me.lbl_menu_title.Name = "lbl_menu_title"
        Me.lbl_menu_title.Size = New System.Drawing.Size(164, 30)
        Me.lbl_menu_title.TabIndex = 0
        Me.lbl_menu_title.Text = "拡売費システム"
        '
        'lnk_logout
        '
        Me.lnk_logout.AutoSize = True
        Me.lnk_logout.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lnk_logout.ForeColor = System.Drawing.Color.Orange
        Me.lnk_logout.LinkColor = System.Drawing.Color.White
        Me.lnk_logout.Location = New System.Drawing.Point(11, 7)
        Me.lnk_logout.Name = "lnk_logout"
        Me.lnk_logout.Size = New System.Drawing.Size(53, 19)
        Me.lnk_logout.TabIndex = 1
        Me.lnk_logout.TabStop = True
        Me.lnk_logout.Text = "ログオフ"
        '
        'lbl_section
        '
        Me.lbl_section.AutoSize = True
        Me.lbl_section.Font = New System.Drawing.Font("Meiryo UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lbl_section.ForeColor = System.Drawing.Color.White
        Me.lbl_section.Location = New System.Drawing.Point(683, 0)
        Me.lbl_section.Name = "lbl_section"
        Me.lbl_section.Size = New System.Drawing.Size(67, 18)
        Me.lbl_section.TabIndex = 2
        Me.lbl_section.Text = "課コード："
        '
        'lbl_user
        '
        Me.lbl_user.AutoEllipsis = True
        Me.lbl_user.AutoSize = True
        Me.lbl_user.Font = New System.Drawing.Font("Meiryo UI", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lbl_user.ForeColor = System.Drawing.Color.White
        Me.lbl_user.Location = New System.Drawing.Point(683, 18)
        Me.lbl_user.Name = "lbl_user"
        Me.lbl_user.Size = New System.Drawing.Size(79, 18)
        Me.lbl_user.TabIndex = 3
        Me.lbl_user.Text = "ログインID："
        '
        'btn_close
        '
        Me.btn_close.Font = New System.Drawing.Font("Meiryo UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btn_close.Location = New System.Drawing.Point(677, 267)
        Me.btn_close.Name = "btn_close"
        Me.btn_close.Size = New System.Drawing.Size(185, 40)
        Me.btn_close.TabIndex = 30
        Me.btn_close.Text = "終了"
        Me.btn_close.UseVisualStyleBackColor = True
        '
        'btn_PaymentApproval
        '
        Me.btn_PaymentApproval.Font = New System.Drawing.Font("Meiryo UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btn_PaymentApproval.Location = New System.Drawing.Point(454, 267)
        Me.btn_PaymentApproval.Name = "btn_PaymentApproval"
        Me.btn_PaymentApproval.Size = New System.Drawing.Size(185, 40)
        Me.btn_PaymentApproval.TabIndex = 29
        Me.btn_PaymentApproval.Text = "支払検証"
        Me.btn_PaymentApproval.UseVisualStyleBackColor = True
        '
        'btn_AccountPayable
        '
        Me.btn_AccountPayable.Font = New System.Drawing.Font("Meiryo UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btn_AccountPayable.Location = New System.Drawing.Point(453, 193)
        Me.btn_AccountPayable.Name = "btn_AccountPayable"
        Me.btn_AccountPayable.Size = New System.Drawing.Size(185, 40)
        Me.btn_AccountPayable.TabIndex = 28
        Me.btn_AccountPayable.Text = "決算確定B・未確定C"
        Me.btn_AccountPayable.UseVisualStyleBackColor = True
        '
        'btn_SalesPromotionStoreMaster
        '
        Me.btn_SalesPromotionStoreMaster.Font = New System.Drawing.Font("Meiryo UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btn_SalesPromotionStoreMaster.Location = New System.Drawing.Point(234, 193)
        Me.btn_SalesPromotionStoreMaster.Name = "btn_SalesPromotionStoreMaster"
        Me.btn_SalesPromotionStoreMaster.Size = New System.Drawing.Size(185, 40)
        Me.btn_SalesPromotionStoreMaster.TabIndex = 27
        Me.btn_SalesPromotionStoreMaster.Text = "拡売費登録店マスタ"
        Me.btn_SalesPromotionStoreMaster.UseVisualStyleBackColor = True
        '
        'btn_ApplicationApproval
        '
        Me.btn_ApplicationApproval.Font = New System.Drawing.Font("Meiryo UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btn_ApplicationApproval.Location = New System.Drawing.Point(19, 193)
        Me.btn_ApplicationApproval.Name = "btn_ApplicationApproval"
        Me.btn_ApplicationApproval.Size = New System.Drawing.Size(185, 40)
        Me.btn_ApplicationApproval.TabIndex = 26
        Me.btn_ApplicationApproval.Text = "申請検証"
        Me.btn_ApplicationApproval.UseVisualStyleBackColor = True
        '
        'btn_PaymentCollation
        '
        Me.btn_PaymentCollation.Font = New System.Drawing.Font("Meiryo UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btn_PaymentCollation.Location = New System.Drawing.Point(453, 130)
        Me.btn_PaymentCollation.Name = "btn_PaymentCollation"
        Me.btn_PaymentCollation.Size = New System.Drawing.Size(185, 40)
        Me.btn_PaymentCollation.TabIndex = 25
        Me.btn_PaymentCollation.Text = "照合・支払"
        Me.btn_PaymentCollation.UseVisualStyleBackColor = True
        '
        'btn_StoreCodeConversionMaster
        '
        Me.btn_StoreCodeConversionMaster.Font = New System.Drawing.Font("Meiryo UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btn_StoreCodeConversionMaster.Location = New System.Drawing.Point(233, 130)
        Me.btn_StoreCodeConversionMaster.Name = "btn_StoreCodeConversionMaster"
        Me.btn_StoreCodeConversionMaster.Size = New System.Drawing.Size(185, 40)
        Me.btn_StoreCodeConversionMaster.TabIndex = 24
        Me.btn_StoreCodeConversionMaster.Text = "店コード変換マスタ"
        Me.btn_StoreCodeConversionMaster.UseVisualStyleBackColor = True
        '
        'btn_AnnualInput
        '
        Me.btn_AnnualInput.Font = New System.Drawing.Font("Meiryo UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btn_AnnualInput.Location = New System.Drawing.Point(18, 130)
        Me.btn_AnnualInput.Name = "btn_AnnualInput"
        Me.btn_AnnualInput.Size = New System.Drawing.Size(185, 40)
        Me.btn_AnnualInput.TabIndex = 23
        Me.btn_AnnualInput.Text = "年間入力"
        Me.btn_AnnualInput.UseVisualStyleBackColor = True
        '
        'btn_PaymentDataOutput
        '
        Me.btn_PaymentDataOutput.Font = New System.Drawing.Font("Meiryo UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btn_PaymentDataOutput.Location = New System.Drawing.Point(677, 67)
        Me.btn_PaymentDataOutput.Name = "btn_PaymentDataOutput"
        Me.btn_PaymentDataOutput.Size = New System.Drawing.Size(185, 40)
        Me.btn_PaymentDataOutput.TabIndex = 22
        Me.btn_PaymentDataOutput.Text = "支払データ出力"
        Me.btn_PaymentDataOutput.UseVisualStyleBackColor = True
        '
        'btn_PaymentDataCreate
        '
        Me.btn_PaymentDataCreate.Font = New System.Drawing.Font("Meiryo UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btn_PaymentDataCreate.Location = New System.Drawing.Point(453, 67)
        Me.btn_PaymentDataCreate.Name = "btn_PaymentDataCreate"
        Me.btn_PaymentDataCreate.Size = New System.Drawing.Size(185, 40)
        Me.btn_PaymentDataCreate.TabIndex = 21
        Me.btn_PaymentDataCreate.Text = "支払データ作成"
        Me.btn_PaymentDataCreate.UseVisualStyleBackColor = True
        '
        'btn_ItemCodeConversionMaster
        '
        Me.btn_ItemCodeConversionMaster.Font = New System.Drawing.Font("Meiryo UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btn_ItemCodeConversionMaster.Location = New System.Drawing.Point(233, 67)
        Me.btn_ItemCodeConversionMaster.Name = "btn_ItemCodeConversionMaster"
        Me.btn_ItemCodeConversionMaster.Size = New System.Drawing.Size(185, 40)
        Me.btn_ItemCodeConversionMaster.TabIndex = 20
        Me.btn_ItemCodeConversionMaster.Text = "商品コード変換マスタ"
        Me.btn_ItemCodeConversionMaster.UseVisualStyleBackColor = True
        '
        'btn_MonthlyApplication
        '
        Me.btn_MonthlyApplication.Font = New System.Drawing.Font("Meiryo UI", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btn_MonthlyApplication.Location = New System.Drawing.Point(16, 67)
        Me.btn_MonthlyApplication.Name = "btn_MonthlyApplication"
        Me.btn_MonthlyApplication.Size = New System.Drawing.Size(185, 40)
        Me.btn_MonthlyApplication.TabIndex = 19
        Me.btn_MonthlyApplication.Text = "月次申請"
        Me.btn_MonthlyApplication.UseVisualStyleBackColor = True
        '
        'LoginF2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(915, 402)
        Me.Controls.Add(Me.btn_close)
        Me.Controls.Add(Me.btn_PaymentApproval)
        Me.Controls.Add(Me.btn_AccountPayable)
        Me.Controls.Add(Me.btn_SalesPromotionStoreMaster)
        Me.Controls.Add(Me.btn_ApplicationApproval)
        Me.Controls.Add(Me.btn_PaymentCollation)
        Me.Controls.Add(Me.btn_StoreCodeConversionMaster)
        Me.Controls.Add(Me.btn_AnnualInput)
        Me.Controls.Add(Me.btn_PaymentDataOutput)
        Me.Controls.Add(Me.btn_PaymentDataCreate)
        Me.Controls.Add(Me.btn_ItemCodeConversionMaster)
        Me.Controls.Add(Me.btn_MonthlyApplication)
        Me.Controls.Add(Me.pnl_title)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "LoginF2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "メニュー-拡売費システム"
        Me.pnl_title.ResumeLayout(False)
        Me.pnl_title.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnl_title As Panel
    Friend WithEvents lbl_menu_title As Label
    Friend WithEvents lnk_logout As LinkLabel
    Friend WithEvents lbl_section As Label
    Friend WithEvents lbl_user As Label
    Friend WithEvents btn_close As Button
    Friend WithEvents btn_PaymentApproval As Button
    Friend WithEvents btn_AccountPayable As Button
    Friend WithEvents btn_SalesPromotionStoreMaster As Button
    Friend WithEvents btn_ApplicationApproval As Button
    Friend WithEvents btn_PaymentCollation As Button
    Friend WithEvents btn_StoreCodeConversionMaster As Button
    Friend WithEvents btn_AnnualInput As Button
    Friend WithEvents btn_PaymentDataOutput As Button
    Friend WithEvents btn_PaymentDataCreate As Button
    Friend WithEvents btn_ItemCodeConversionMaster As Button
    Friend WithEvents btn_MonthlyApplication As Button
End Class
