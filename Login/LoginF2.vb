﻿Imports Common

Public Class LoginF2

#Region "終了ボタン押下"
    ''' <summary>
    ''' 終了ボタン押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btn_close_Click(sender As Object, e As EventArgs) Handles btn_close.Click
        Try
            Me.Close()
        Catch ex As Exception
            ' ログ
            Common.LogHelper.writeLog(ex.Message + System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
#End Region
#Region "メニュータイトル表示内容"
    ''' <summary>
    ''' メニュータイトル表示内容
    ''' </summary>
    Private Sub SetTitleInfor()
        Dim init = New Common.IniFileReaderHelper
        ' 課情報
        lbl_section.Text = init.GetIniValue("USERINFO", "login_section_code", "") + ":" + init.GetIniValue("USERINFO", "login_section_name", "")
        ' 担当情報
        lbl_user.Text = init.GetIniValue("USERINFO", "login_user_id", "") + ":" + init.GetIniValue("USERINFO", "login_user_name", "")

        ' タイトル色
        If "1".Equals(init.GetIniValue("DEBUG", "isDebug", "")) Then
            ' 開発
            pnl_title.BackColor = Color.DimGray

            lbl_menu_title.Text = "拡売費システム（開発）"
        Else
            ' 本番
            pnl_title.BackColor = SystemColors.MenuHighlight
            lbl_menu_title.Text = "拡売費システム"
        End If
    End Sub
#End Region
#Region "フォームロード"
    ''' <summary>
    ''' フォームロード
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub LoginF2_Load(sender As Object, e As EventArgs) Handles Me.Load
        ' タイトルで表示する情報設定
        SetTitleInfor()
        ' 各ボタン設定
        InitMenuButton()
        ' アクティブボタンデフォルト選択
        SetActiveButton()
    End Sub
#End Region
#Region "ボタン有効性設定"
    ''' <summary>
    ''' ボタン設定
    ''' </summary>
    Private Sub InitMenuButton()
        ' ログインユーザーのロールよりアクセスできる画面リスト取得
        Dim init = New Common.IniFileReaderHelper
        Dim ds As DataSet
        Dim accessPages As Collection = New Collection
#Region "ROLEよりアクセルできるページリスト取得"
        Dim sql = "SELECT 
	                    FUNCTION_ID,
	                    INVALID_FLG
                    FROM TM_FUNCTIONS_BY_ROLE
                    WHERE 
	                    ROLE_ID=@roleId 
                    AND 
	                    LOGICAL_DELETE_FLG='0'"
        Using command = New SqlCommandManager(sql)
            command.AddParameter("roleId", SqlDbType.VarChar, init.GetIniValue("USERINFO", "login_role_id", ""))
            ds = LoginM.da.GetDataSet(command.GetCommand())
            ' データが存在する場合コレクション置く
            If Not IsNothing(ds) And ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    accessPages.Add(row.Item("FUNCTION_ID").ToString.Replace(".exe", ""), row.Item("FUNCTION_ID").ToString.Replace(".exe", ""))
                Next
            End If
        End Using
#End Region
#Region "各ボタンの有効性設定"
        For Each ctl As Control In Me.Controls
            If ctl.GetType Is GetType(Button) Then
                If accessPages.Contains(DirectCast(ctl, Button).Name.Substring(4)) Or "btn_close".Equals(DirectCast(ctl, Button).Name) Then
                    DirectCast(ctl, Button).Enabled = True
                Else
                    DirectCast(ctl, Button).Enabled = False
                End If
            End If

        Next
#End Region
    End Sub
#End Region
#Region "ログオフ"
    ''' <summary>
    ''' ログオフ
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub lnk_logout_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnk_logout.LinkClicked
        Try

            Me.Close()
            Me.Dispose()
            ' ログ画面起動
            Dim loginF1 = New LoginF1
            loginF1.ShowDialog()
        Catch ex As Exception
            Common.LogHelper.writeLog(Application.ProductName + ex.Message + System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try
    End Sub
#End Region
#Region "アクティブボタン設定"
    ''' <summary>
    ''' アクティブボタン設定
    ''' </summary>
    Private Sub SetActiveButton()
        Select Case True
            Case btn_MonthlyApplication.Enabled ' 月次申請
                btn_MonthlyApplication.Select()
                Exit Select
            Case btn_ItemCodeConversionMaster.Enabled ' 商品コード変換マスタ
                btn_ItemCodeConversionMaster.Select()
                Exit Select
            Case btn_PaymentDataCreate.Enabled ' 支払データ作成
                btn_PaymentDataCreate.Select()
                Exit Select
            Case btn_PaymentDataOutput.Enabled ' 支払データ出力
                btn_PaymentDataOutput.Select()
                Exit Select
            Case btn_AnnualInput.Enabled ' 年間入力
                btn_AnnualInput.Select()
                Exit Select
            Case btn_StoreCodeConversionMaster.Enabled ' 店コード変換マスタ
                btn_StoreCodeConversionMaster.Select()
                Exit Select
            Case btn_PaymentCollation.Enabled ' 照合・支払
                btn_PaymentCollation.Select()
                Exit Select
            Case btn_ApplicationApproval.Enabled ' 申請検証
                btn_ApplicationApproval.Select()
                Exit Select
            Case btn_SalesPromotionStoreMaster.Enabled ' 拡売費登録店マスタ
                btn_SalesPromotionStoreMaster.Select()
                Exit Select
            Case btn_AccountPayable.Enabled ' 決算確定Ｂ・未確定Ｃ
                btn_AccountPayable.Select()
                Exit Select
            Case btn_PaymentApproval.Enabled ' 支払検証
                btn_PaymentApproval.Select()
                Exit Select
            Case btn_close.Enabled ' 終了
                btn_close.Select()
                Exit Select
        End Select
    End Sub
#End Region
#Region "メニューの各ボタンクリック"
    ''' <summary>
    ''' メニューの各ボタンクリック
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btn_MonthlyApplication_Click(sender As Object, e As EventArgs) Handles btn_MonthlyApplication.Click, btn_ItemCodeConversionMaster.Click, btn_PaymentDataCreate.Click, btn_PaymentDataOutput.Click, btn_AnnualInput.Click, btn_StoreCodeConversionMaster.Click, btn_PaymentCollation.Click, btn_ApplicationApproval.Click, btn_SalesPromotionStoreMaster.Click, btn_AccountPayable.Click, btn_PaymentApproval.Click
        Dim btnName As String = CType(sender, Button).Name
        Dim args As String = String.Empty
        Try
            Dim common = New Common.Common
            Me.Hide()
            ' コマンドランのargs作成
            Select Case True
                Case "btn_MonthlyApplication".Equals(btnName) ' 月次申請
                    args = "1 2 3"
                    Exit Select
                Case "btn_ItemCodeConversionMaster".Equals(btnName) ' 商品コード変換マスタ
                    args = ""
                    Exit Select
                Case "btn_PaymentDataCreate".Equals(btnName) ' 支払データ作成
                    args = ""
                    Exit Select
                Case "btn_PaymentDataOutput".Equals(btnName) ' 支払データ出力
                    args = ""
                    Exit Select
                Case "btn_AnnualInput".Equals(btnName) ' 年間入力
                    args = ""
                    Exit Select
                Case "btn_StoreCodeConversionMaster".Equals(btnName) ' 店コード変換マスタ
                    args = ""
                    Exit Select
                Case "btn_PaymentCollation".Equals(btnName) ' 照合・支払
                    args = ""
                    Exit Select
                Case "btn_ApplicationApproval".Equals(btnName) ' 申請検証
                    args = ""
                    Exit Select
                Case "btn_SalesPromotionStoreMaster".Equals(btnName) ' 拡売費登録店マスタ
                    args = "3"
                    Exit Select
                Case "btn_AccountPayable".Equals(btnName) ' 決算確定Ｂ・未確定Ｃ
                    args = ""
                    Exit Select
                Case "btn_PaymentApproval".Equals(btnName) ' 支払検証
                    args = ""
                    Exit Select
            End Select

            Dim p As System.Diagnostics.Process =
                System.Diagnostics.Process.Start(String.Concat(common.GetexePath, btnName.Substring(4), ".exe"), args)
            p.WaitForExit()
            Me.Show()
        Catch ex As Exception
            Me.Show()
            Common.LogHelper.WriteLogAndShowMsg(ex)
        End Try
    End Sub
#End Region
End Class