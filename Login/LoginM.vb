﻿
Module LoginM
    Friend da As Common.DataAccess
    <STAThread()>
    Sub Main(args As String())
        Try
            Using da
                da = New Common.DataAccess
                da.Open()
                ' 二重起動チェック
                Dim mt As New Common.Common
                If mt.Init() = False Then Exit Sub

                ' isDebug=1の場合
                '                ログイン画面表示
                ' isDebug=0の場合
                '                AD確認↓
                '                   成功→メニュー画面表示
                '                   失敗→ログイン画面表示
                Dim init As New Common.IniFileReaderHelper
                ' デバッグモード取得
                Dim isDebug As String = init.GetIniValue("DEBUG", "isDebug", "1")
                Dim user As New Common.UserHelper

                ' ADユーザー情報がない場合或いはデバッグの場合、ログイン画面起動
                If "1".EndsWith(isDebug) Or Len(user.GetuserInfo.BasicUser.Id) = 0 Then
                    'ログイン画面起動
                    'Application.EnableVisualStyles()
                    'Application.SetCompatibleTextRenderingDefault(False)
                    'loginF1 = New LoginF1
                    'Application.Run(loginF1)
                    Dim loginF1 As New LoginF1
                    loginF1.ShowDialog()

                    If loginF1 IsNot Nothing Then
                        loginF1.Dispose()
                        loginF1 = Nothing
                    End If
                Else
                    ' ユーザー情報をinitファイルに書き込み
                    Dim res As Boolean = user.SetUserInfo(user.GetuserInfo)
                    If Not res Then
                        Common.LogHelper.WriteLogAndShowMsg(New Exception("ユーザー情報書きこみエラー"), "ユーザー情報をシステム初期化initファイルに生成失敗しました。")
                    Else
                        ' メニュー画面起動
                        Dim LoginF2 As New LoginF2
                        LoginF2.ShowDialog()
                        If LoginF2 IsNot Nothing Then
                            LoginF2.Dispose()
                            LoginF2 = Nothing
                        End If
                    End If
                End If
            End Using
        Catch ex As Exception
            'ログ処理用の共通クラスを実行（ログ出力＆Messagebox）
            Common.LogHelper.WriteLogAndShowMsg(ex)
        Finally
            da.Close()
            Common.LogHelper.writeLog(Application.ProductName + " 終了 " + System.Reflection.MethodBase.GetCurrentMethod().Name)
        End Try

    End Sub
End Module
